New:
- Added German translation for F-Droid website
- Added setting for quick entry mode (hidden until fully implemented)

Fixes:
- Typo in english Date/Time Format settings description
- Added new mime types "application/csv" and "text/csv" to restore function to cater for different OS versions reporting different mime types for csv files
- Corrected slightly off success message during data export (Backup)
