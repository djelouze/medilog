New:

Fixes:
- Significantly improved robustness of import/restore process
- Enable/disable Biometric logon setting based on hardware availability
- Set CSV delimiter based on language on first launch (";" for German, "," for the rest)
- Fixed CSV delimiter setting input. Only , and ; are accepted