# MediLog
![MediLog](./app/src/main/ic_launcher.png)

Android App to easily capture blood pressure and body weight

**Note: If you are upgrading from versions < 1.5 please make sure you export your data first. V 1.5 will create a new database!**

## Key features

### Tracker free
I wrote this app because I don't want to hand over my health data to any tracking company out there, but also because I wanted to learn how to write an app for Android. Have a look at the result and tell me if I acomplished my goal :-)
In any case, no telemetry, no crash logs or anything similar is sent to anyone.

### Manual data entry
Data input is purposely kept to manual, but aimed to make entry as quick and simple as possible. 
I have no intention to add links to wifi/bluetooth enabled measurement devices as these will introduce 3rd parties one doesn't control. 

### Data export
Data can be imported and exported for backup and transfer reasons. File format is CSV which is understood by almost any application which works with structured data. E.g. Excel, LibreOffice Calc, databases, etc.

The app allows to create PDF reports to print/share with your doctor/medical staff. 
Be aware that this is sensitive data, so please handle with due care and enable password protection / encryption for the exported files.

### Multi language
Currently the app supports English and German, if someone volunteers as translator, more languages can be added

### Installation

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.zell_mbc.medilog/)

From my website: https://zell-mbc.com/documents/app-release.apk

Or clone and compile from here

### Removal
Like with all Android applications, via a long click on the app icon.


**Uninstalling the application will delete the database! Unrecoverable! Make sure you export your data first!**


## Privacy Policy

### Stored data
Data entered by the user is stored inside an encrypted (v.1.7.5+) SQLite database. 

**To keep the input process as simple and fast as possible, the app/your data is not protected with an additional password. If your device has support for Biometric (Fingerprint) make sure to enable it. Otherwise, if someone is able to unlock your device they can access your health data!**

The app supports storing backups in encrypted ZIP files. Make sure you turn encryption on by setting a ZIP password in the settings dialog. Make sure you remember the password.
The app allows to send protected files so you can share over unprotected media (eg. email) without the risk of your data getting in the wrong hands.


### Required permissions

- WRITE_EXTERNAL_STORAGE : Required to export CSV backup files
- READ_EXTERNAL_STORAGE : Required to import CSV files


### Tracking and Libraries
This application does not use any tracking tools and does not run advertising.

3rd party libraries used are the below:

- AndroidPlot: For the charts
- Takisoft.fix: To fix an Android preference dialog bug.
- Zip4J: For handling password protected ZIP files
- SQLCipher: To encrypt the SQLite database

## Contact

- Matrix: #medilog:zell-mbc.com
- eMail: medilog@zell-mbc.com

## Languages

- English
- German

## Donations
If you feel like it…

[<img src="https://www.paypalobjects.com/webstatic/de_DE/i/de-pp-logo-100px.png" border="0"
    alt="PayPal Logo">](https://www.paypal.com/paypalme/thomaszellmbc)

[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bitcoin_logo.svg/252px-Bitcoin_logo.svg.png" border="0"
height="20" alt="Bitcoin Logo">](https://live.blockcypher.com/btc/address/1EUrRpjDAGgpS8J46tmsVWKbgNqAv7rWC7/)


## Screenshots
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/list1.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/list2.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/chart1.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/chart2.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Settings.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/About.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/PDFReport.png)

## Changelog
[Change log](ChangeLog.md)


