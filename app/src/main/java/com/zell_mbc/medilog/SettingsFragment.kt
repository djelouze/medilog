package com.zell_mbc.medilog

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.Spanned
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.takisoft.preferencex.EditTextPreference
import com.takisoft.preferencex.PreferenceFragmentCompat


class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        class DelimiterInputFilter(s: String): InputFilter {
        private var filterString = ""
        private var filterHint = ""

            init{
                this.filterString = "$s"
                for (c in s) this.filterHint = this.filterHint + c + " " // Add blanks for better visibility
            }

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                try
                {
                    val input = (dest.subSequence(0, dstart).toString() + source + dest.subSequence(dend, dest.length))
                    if (filterString.indexOf(input) > 0) return null
                    else
                        if (input.isNotEmpty()) Snackbar.make(requireView(), requireContext().getString(R.string.allowedDelimiters) + " $filterHint", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                }
                catch (nfe: NumberFormatException) {}
                return ""
            }
        }

        val editTextPreference = preferenceManager.findPreference<EditTextPreference>("delimiter")
        if (editTextPreference != null) {
            editTextPreference.setOnBindEditTextListener { editText ->
                val filterArray = arrayOfNulls<InputFilter>(2)
                filterArray[0] = LengthFilter(1)
                filterArray[1] = DelimiterInputFilter(this.getString(R.string.acceptableCsvSeparators))
                editText.setFilters(filterArray)
             }

            editTextPreference.setOnPreferenceChangeListener(object : Preference.OnPreferenceChangeListener {
                override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
                    var rtnval = true
                    if (newValue.toString().isEmpty()) {
                        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                        builder.setTitle(getString(R.string.invalidInput))
                        builder.setMessage(getString(R.string.emptySeparator))
                        builder.setPositiveButton(android.R.string.ok, null)
                        builder.show()
                        rtnval = false
                    }
                    return rtnval
                }
            })
        }

        // Check if biometric device  exists, if not remove biometric setting in settings activity and set authenticated to always true
        val biometricHelper = BiometricHelper(requireContext())
        val canAuthenticate = biometricHelper.canAuthenticate(false)
        if (canAuthenticate != 0) {
            val p: Preference? = findPreference(SettingsActivity.KEY_PREF_BIOMETRIC)
            if (p != null) p.isEnabled = false
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}