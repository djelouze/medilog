package com.zell_mbc.medilog

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE

class BiometricHelper internal constructor(var context: Context) {
    var biometricManager: BiometricManager

    fun canAuthenticate(notify: Boolean): Int {
        var ret = -9999 // Unknown error
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
 //               Log.d(context.getString(R.string.app_name), "App can authenticate using biometrics.")
                return 0
            }
            BIOMETRIC_ERROR_NO_HARDWARE -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noBiometricHardware))
                if (notify) {
                    Toast.makeText(context, context.getString(R.string.noBiometricHardware), Toast.LENGTH_LONG).show()
                }
                return -1
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
//                Log.e(context.getString(R.string.app_name), context.getString(R.string.biometricHardwareUnavailable))
                run { Toast.makeText(context, context.getString(R.string.biometricHardwareUnavailable), Toast.LENGTH_LONG).show() }
                return -2
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noCredentials))
                if (notify) {
                    Toast.makeText(context, context.getString(R.string.noCredentials), Toast.LENGTH_LONG).show()
                }
                return -3
            }
        }
        return ret
    }

    fun hasHardware(notify: Boolean): Boolean {
        when (biometricManager.canAuthenticate()) {
            BIOMETRIC_ERROR_NO_HARDWARE -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noBiometricHardware))
                if (notify) {
                    Toast.makeText(context, context.getString(R.string.noBiometricHardware), Toast.LENGTH_LONG).show()
                return false
                }
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.biometricHardwareUnavailable))
                if (notify) {
                    Toast.makeText(context, context.getString(R.string.biometricHardwareUnavailable), Toast.LENGTH_LONG).show()
                }
                return false
            }
        }
        return true
    }

    init {
        biometricManager = BiometricManager.from(context)
    }
}