package com.zell_mbc.medilog.diary

import android.app.AlertDialog
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.*
import kotlinx.android.synthetic.main.diary_tab.*
import java.util.*
import androidx.lifecycle.Observer
import java.text.DateFormat

class DiaryFragment : Fragment(), DiaryListAdapter.ItemClickListener {
    private lateinit var viewModel: DiaryViewModel
    private var adapter: DiaryListAdapter? = null
    private var quickEntry = true

    private fun addItem() {
        if (!quickEntry) {
            val tmpDiary = "temporaryDiaryEntry-YouShouldNeverSeeThis"
            val tmpItem = Diary(0, Date().time, "", tmpDiary)
            viewModel.insert(tmpItem)
            var newItem = viewModel.getItem(tmpDiary)
            var turns = 100
            while (newItem == null && turns > 0) {
                Log.d("Wait for Insert to complete", "Turns $turns")
                turns--
                newItem = viewModel.getItem(tmpDiary)
            }
            if (newItem != null) editItem(newItem._id)
            else Snackbar.make(requireView(), "No entry with tmp Diary found!"  , Snackbar.LENGTH_LONG).setAction("Action", null).show()

            return
        }

        // Check empty variables
        val value = etDiary.text.toString()
        if (value.isEmpty()) {
            val v = view
            if (v != null) {
                Snackbar.make(v, getString(R.string.diaryMissing), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
            return
        }

        val item = Diary(0, Date().time, "", value)

        viewModel.insert(item)
        if (viewModel.filterActive && (viewModel.filterEnd < item.timestamp)) Snackbar.make(requireView(), getString(R.string.filteredOut), 4000).setAction("Action", null).show()
//        else Snackbar.make(requireView(), getString(R.string.word_new) + " " + viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_saved), Snackbar.LENGTH_LONG).setAction("Action", null).show()

        etDiary.setText("")
    }

    private fun editItem(index: Int) {
        val intent = Intent(requireContext(), DiaryEditActivity::class.java)
        val extras = Bundle()
        val item = viewModel.getItem(index)
        if (item != null) {
            extras.putInt("ID", item._id)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        Snackbar.make(requireView(), viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    fun notifyDataSetChanged() {
        adapter?.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
 //       Log.d("DiaryFragment onCreateView : ", "Start" + inflater + " parent " + parent + " bundle " + savedInstanceState)
        return inflater.inflate(R.layout.diary_tab, parent, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val addButton = button_addDiary as FloatingActionButton
        quickEntry = sharedPref.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)

        when (colourStyle) {
            this.getString(R.string.green) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }

        // Hide quick entry fields if needed
        if (!quickEntry) etDiary.visibility = View.GONE
        else etDiary.visibility = View.VISIBLE

        // set up the RecyclerView
        val layoutManager = LinearLayoutManager(requireContext())
        rvDiaryList.setLayoutManager(layoutManager)

        adapter = DiaryListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
        viewModel.repository.items.observe(requireActivity(), Observer { diary -> diary?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        rvDiaryList.adapter = adapter

        // Respond to click events
        addButton.setOnClickListener { addItem() }
    }

    override fun onItemClick(view: View?, position: Int) {
        val c = context
                ?: //           Log.d("Debug onViewCreated: ", "Empty Context");
                return
        val alertDialogBuilder = AlertDialog.Builder(c)
        val item: Diary? = viewModel.items.value!![position]
        item ?: return
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        alertDialogBuilder.setTitle(getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
        // set dialog message
        alertDialogBuilder
                .setMessage("What do you want to do?")
                .setCancelable(false)
                .setPositiveButton("Delete") { dialog, _ ->
                    deleteItem(item._id)
                    dialog.cancel()
                }
                .setNeutralButton("Cancel") { dialog, _ -> dialog.cancel() }
                .setNegativeButton("Edit") { dialog, _ ->
                    editItem(item._id)
                    dialog.cancel()
                }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}