package com.zell_mbc.medilog.diary

import androidx.lifecycle.LiveData
import com.zell_mbc.medilog.base.DataRepository
import com.zell_mbc.medilog.Diary
import com.zell_mbc.medilog.DiaryDao

class DiaryRepository(private val dao: DiaryDao, private val filterStart: Long, private val filterEnd: Long): DataRepository() {

    val items: LiveData<List<Diary>> = dao.getDiaryItemsFiltered(filterStart,filterEnd)

    override fun getItems(order: String): List<Diary> {
       if (order.equals("ASC")) return dao.getDiaryItemsASC()
        else                    return dao.getDiaryItemsDESC()
    }

    override fun getItemsFiltered(order: String, filterStart: Long, filterEnd: Long): List<Diary> {
        if (order.equals("ASC")) return dao.getDiaryItemsASCFiltered(filterStart, filterEnd)
        else                    return dao.getDiaryItemsDESCFiltered(filterStart, filterEnd)
    }

    override fun getSizeUnfiltered(): Int { return dao.getSizeUnfiltered() }
    override fun getSizeFiltered(filterStart: Long, filterEnd: Long): Int { return dao.getSizeFiltered(filterStart, filterEnd) }
    override fun getItem(index: Int): Diary = dao.getItem(index)
    fun getItem(diaryValue: String): Diary = dao.getItem(diaryValue)

    suspend fun insert(diary: Diary) { dao.insert(diary) }
    suspend fun update(diary: Diary) { dao.update(diary) }
    suspend fun delete(diary: Diary) { dao.delete(diary) }
    suspend fun delete(diaryValue: String) { dao.delete(diaryValue) }

    override suspend fun delete(id: Int) { dao.delete(id) }
    override suspend fun deleteAll() { dao.deleteAll() }
    override suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) { dao.deleteAllFiltered(filterStart, filterEnd) }
}