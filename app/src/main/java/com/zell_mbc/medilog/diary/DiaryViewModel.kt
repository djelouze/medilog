package com.zell_mbc.medilog.diary

import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.DataViewModel
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.IOException
import java.lang.StringBuilder
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DiaryViewModel(application: Application): DataViewModel(application) {
    override val fileName = "Diary.csv"
    override var itemName = app.getString(R.string.diary)
    override val filterStartPref = "DIARYFILTERSTART"
    override val filterEndPref = "DIARYFILTEREND"

    override lateinit var repository: DiaryRepository
    lateinit var dao: DiaryDao
    lateinit var items: LiveData<List<Diary>>

    override fun init() {
        super.init()

        dao = MediLogDB.getDatabase(app, viewModelScope).diaryDao()
        repository = DiaryRepository(dao, filterStart, filterEnd)
        items = repository.items
    }

    fun insert(diary: Diary) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(diary)
    }

    fun update(diary: Diary) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(diary)
    }

    fun delete(diaryValue: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(diaryValue)
    }

    override fun getItem(id: Int): Diary? {
        var item: Diary? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun getItem(diaryValue: String): Diary? {
        var item: Diary? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(diaryValue) }
            j.join() }
        return item
    }

    override fun getItems(order: String): List<Diary> {
        getFilter()
        lateinit var items: List<Diary>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            if (filterActive) items = repository.getItemsFiltered(order, filterStart, filterEnd)
            else items = repository.getItems(order) }
            j.join() }
        return items
    }

// --------

    fun csvToItem(w: Diary, csv: String): Int {
        val data = csv.split(separator!!.toRegex()).toTypedArray()

        // At a minimum 2 values need to be present, comment is optional
        if (data.size < 2) {
            return -1
        }
        try {
            val date: Date? = csvPattern.parse(data[0].trim())
            if (date != null) w.timestamp =  date.time
        } catch (e: ParseException) {
            return -2
        }
        try {
            w.diary = data[1]
        } catch (e: NumberFormatException) {
            return -3
        }

        // Comment not always available
        w.comment = if (data.size == 3) {
            data[2]
        } else {
            ""
        }
        return 0
    }


    override suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int {
        val tmpItems = ArrayList<Diary>()
        if (reader == null) {
            return 0
        }
        try {
            var tmpLine = ""
            var showError = true
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                val item = Diary(0, 0,"", "") // Start with empty item
                val err = csvToItem(item, line!!)
                if (err < 0) {
                    if (showError) {
                        Handler(Looper.getMainLooper()).post {
                            Toast.makeText(app, "Error " + err + " importing line " + lineNo + ", '" + tmpLine + "'", Toast.LENGTH_LONG).show()
                        }
                        showError = false // Show only first error to avoid pushing hundreds of similar errors
                    }
                    continue
                }
                tmpItems.add(item)
            }
        } catch (e: IOException) {
            return -1
        }

        return transferItems(tmpItems, replaceData)
    }


    suspend fun transferItems(tmpItems: ArrayList<Diary>, replaceData: Boolean): Int {
        val records = tmpItems.size
        if (records == 0) return -2     // Delete only after a successful import

        // Delete only after a successful import
        if (replaceData) {
            deleteAllUnfiltered()
            // Wait until DB is empty
            var i = 5
            while (repository.getSizeUnfiltered() > 0 && i > 0) {
                //                   Log.d("SuspendedInsert", "Avoid race condition -> Wait 100ms")
                delay(100L)
                Log.d("--------------- Debug", "Delay:" + i--)
                if (i == 0) {
                    // If after 10 rounds the DB is not empty something odd is going on
                    Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Error! Unable to delete all old records. ", Toast.LENGTH_LONG).show() }
                }
            }
        }
        for (item in tmpItems) {
            val job = insert(item)
            job.join()
        }
        return records
    }


    override fun csvData(filtered: Boolean): String {
        val sb = StringBuilder()

        // CSV header
        sb.append(app.getString(R.string.date) + separator + app.getString(R.string.diary) + System.getProperty("line.separator"))

        // Data
        val items = getItems("DESC")
        for (item in items) {
            sb.append(csvPattern.format(item.timestamp) + separator + item.diary + separator + item.comment + System.getProperty("line.separator"))
            }
        return sb.toString()
    }


    override fun createPDF(): PdfDocument? {
        if (getSizeFiltered() == 0) {
            Toast.makeText(app, app.getString(R.string.diary) + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }
        val userName = sharedPref.getString(SettingsActivity.KEY_PREF_USER, "")
        val document = PdfDocument()


//        int tab1 = 80;
        val pdfDataTab = pdfTimeTab + 70
        var pageNumber = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas

        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.color = Color.BLACK
        val paintRed = Paint()
        paintRed.color = Color.RED
        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true

        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        // Draw header
        var headerText = app.getString(R.string.bpReportTitle)
        if (userName != null) headerText = headerText + " " + app.getString(R.string.forString) + " " + userName

        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        i = pdfDataTop
        // Print header in bold
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.diary), pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)

        // ------------
        // Todo: Line breaks
        val items = getItems("ASC")
        for (item in items) {
            i += pdfLineSpacing
            // Start new page
            if (i > pdfDataBottom) {
                document.finishPage(page)
                pageNumber++

                // crate a A4 page description
                pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
                page = document.startPage(pageInfo)
                canvas = page.canvas
                canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderDateColumn.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                i = pdfDataTop
                canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.diary), pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
                // ------------
                i += pdfLineSpacing
            }
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(item.diary, pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
        }
        // finish the page
        document.finishPage(page)
        return document
    }
}
