package com.zell_mbc.medilog.diary

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import kotlinx.android.synthetic.main.diaryeditform.*
import kotlinx.android.synthetic.main.diaryeditform.button_saveData
import kotlinx.android.synthetic.main.diaryeditform.etDate
import kotlinx.android.synthetic.main.diaryeditform.etTime
import kotlinx.android.synthetic.main.weighteditform.*
import java.text.DateFormat
import java.util.*

class DiaryEditFragment : Fragment() {
    private val viewModel: DiaryViewModel by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })
    var _id: Int = 0
    private val tmpDiary = "temporaryDiaryEntry-YouShouldNeverSeeThis"
    private val timestampCal = Calendar.getInstance()

    private fun saveItem() {
        val editItem = viewModel.getItem(_id)
        if (editItem == null) {
            Snackbar.make(requireView(), "Unknown error!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Check empty variables
        val value = etDiary.text.toString()
        if (value.isEmpty()) {
            Snackbar.make(requireView(), getString(R.string.diaryMissing), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        editItem.diary = value
        editItem.timestamp = timestampCal.timeInMillis

        viewModel.update(editItem)

        Snackbar.make(requireView(),  requireContext().getString(R.string.itemUpdated), Snackbar.LENGTH_LONG).setAction("Action", null).show()
        requireActivity().onBackPressed()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.diaryeditform, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = button_saveData as FloatingActionButton

        when (colourStyle) {
            this.getString(R.string.green) -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }

        viewModel.init()
        val editItem = viewModel.getItem(_id)

        // Check if we are really editing or if this is a new value
        if (editItem?.diary == tmpDiary) {
            this.etDiary.setText("")
        }
        else
            this.etDiary.setText(editItem?.diary.toString())

        this.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem?.timestamp)
        this.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem?.timestamp)

        // Respond to click events
        val button_saveData: FloatingActionButton = view.findViewById(R.id.button_saveData)
        button_saveData.setOnClickListener { saveItem() }

        // ---------------------
        // Date/Time picker section
        if (editItem != null) {
            timestampCal.timeInMillis = editItem.timestamp
        }

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { view, hour, minute ->
            timestampCal.set(Calendar.HOUR, hour)
            timestampCal.set(Calendar.MINUTE, minute)

            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

//        btDatePicker!!.setOnClickListener(object : View.OnClickListener {
        etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }
        // Make sure first field is highlighted and keyboard is open
        this.etDiary.requestFocus()
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.delete(tmpDiary)
    }

    fun newInstance(i: Int): DiaryEditFragment? {
        val f = DiaryEditFragment()
        // Supply index input as an argument.
        f._id = i
      //  Log.d("--------------- Debug", "NewInstance Index: " + _id)
        return f
    }
}