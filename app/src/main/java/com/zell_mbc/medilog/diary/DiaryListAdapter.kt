package com.zell_mbc.medilog.diary

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.Diary
import java.text.DateFormat
import java.text.SimpleDateFormat


class DiaryListAdapter internal constructor(context: Context) : RecyclerView.Adapter<DiaryListAdapter.DiaryViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clickListener: ItemClickListener? = null

    private var items = emptyList<Diary>() // Cached copy of diarys, no ide why I need it

    private val textSize: Float
    private var dateFormat: DateFormat
    private var timeFormat: DateFormat

    private val highlightValues: Boolean

    inner class DiaryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
        val tvDiary: TextView = itemView.findViewById(R.id.tvDiary)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            tvDate.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvDiary.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiaryViewHolder {
        val itemView = inflater.inflate(R.layout.diaryview_row, parent, false)
        return DiaryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DiaryViewHolder, position: Int) {
        val current = items[position]
        holder.tvDate.text = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
        holder.tvDiary.text = current.diary
    }

    internal fun setItems(items: List<Diary>) {
        this.items = items
        notifyDataSetChanged()
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
        textSize = java.lang.Float.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, "15")!!)
        highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
    }
}
