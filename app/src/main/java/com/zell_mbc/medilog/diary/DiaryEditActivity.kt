package com.zell_mbc.medilog.diary

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.weight.WeightEditFragment
import java.util.*

class DiaryEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent: Intent = intent
        val id: Int = intent.getIntExtra("ID", 0)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        when (colourStyle) {
            this.getString(R.string.green) -> theme.applyStyle(R.style.AppThemeGreenBar, true)
            this.getString(R.string.blue)  -> theme.applyStyle(R.style.AppThemeBlueBar, true)
            this.getString(R.string.red)   -> theme.applyStyle(R.style.AppThemeRedBar, true)
        }

        val f = DiaryEditFragment().newInstance(id)
        if (f != null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(android.R.id.content, f)
                    .commit()
        }
    }
}