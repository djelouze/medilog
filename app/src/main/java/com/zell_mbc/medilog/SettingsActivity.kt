package com.zell_mbc.medilog

import android.os.Bundle
import androidx.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.bloodpressure.BloodPressureHelper


class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val colourStyle = sharedPref.getString(KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        when (colourStyle) {
            this.getString(R.string.green) -> theme.applyStyle(R.style.AppThemeGreenBar, true)
            this.getString(R.string.blue)  -> theme.applyStyle(R.style.AppThemeBlueBar, true)
            this.getString(R.string.red)   -> theme.applyStyle(R.style.AppThemeRedBar, true)
        }
        supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
    }

    public override fun onStop() {
        super.onStop()
        // Check validity of bp values
        BloodPressureHelper(this)
    }

    companion object {
        const val KEY_PREF_DELIMITER = "delimiter"
        const val KEY_PREF_WEIGHTUNIT = "weightUnit"
        const val KEY_PREF_COLOUR = "checkBox_setColour"
        const val KEY_PREF_showWeightTab = "showWeightTab"
        const val KEY_PREF_showBloodPressureTab = "showBloodPressureTab"
        const val KEY_PREF_showDiaryTab = "showDiaryTab"
        const val KEY_PREF_TEXT_SIZE = "listTextSize"
        const val KEY_PREF_USER = "userName"
        const val KEY_PREF_hyperGrade3 = "grade3"
        const val KEY_PREF_hyperGrade2 = "grade2"
        const val KEY_PREF_hyperGrade1 = "grade1"
        const val KEY_PREF_weightThreshold = "weightThreshold"
        const val KEY_PREF_WEIGHT_LINEAR_TRENDLINE = "cb_weightLinearTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE = "cb_weightMovingAverageTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE = "et_weightMovingAverageSize"
        const val KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE = "cb_bloodPressureLinearTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE = "cb_bloodPressureMovingAverageTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE = "et_bloodPressureMovingAverageSize"
        const val KEY_PREF_weightDayStepping = "checkBox_weightDayStepping"
        const val KEY_PREF_bpDayStepping = "checkBox_bpDayStepping"
        const val KEY_PREF_weightBarChart = "checkBox_weightBarChart"
        const val KEY_PREF_bpBarChart = "checkBox_bpBarChart"
        const val KEY_PREF_ZIPBACKUP = "zipBackup"
        const val KEY_PREF_COLOUR_STYLE = "colourStyle"
        const val KEY_PREF_PASSWORD = "zipPassword"
        const val KEY_PREF_BIOMETRIC = "enableBiometric"
        const val KEY_PREF_SHOWTHRESHOLDS = "checkBox_showThresholds"
        const val KEY_PREF_SHOWGRID = "checkBox_showGrid"
        const val KEY_PREF_SHOWLEGEND = "checkBox_showLegend"
        const val KEY_PREF_SHOWPULSE = "cb_showPulse"
        const val KEY_PREF_QUICKENTRY = "quickEntry"
    }
}