package com.zell_mbc.medilog

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.room.*
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.zell_mbc.medilog.SQLCipherUtils.getDatabaseState
import kotlinx.coroutines.CoroutineScope
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory
import java.util.*

abstract class DataItem {
    abstract val _id: Int
    abstract var timestamp: Long
    abstract var comment: String
}

@Entity(tableName = "weight",indices = arrayOf(Index(value = ["timestamp"])))
data class Weight(
        @PrimaryKey(autoGenerate = true) override val _id: Int,
        override var timestamp: Long,
        override var comment: String = "",
        var weight: Float = 0f
): DataItem()

@Entity(tableName = "bloodpressure",indices = arrayOf(Index(value = ["timestamp"])))
data class BloodPressure(
        @PrimaryKey(autoGenerate = true) override val _id: Int,
        override var timestamp: Long,
        override var comment: String = "",
        var sys: Int,
        var dia: Int,
        var pulse: Int
): DataItem()

@Entity(tableName = "diary",indices = arrayOf(Index(value = ["timestamp"])))
data class Diary(
        @PrimaryKey(autoGenerate = true) override val _id: Int,
        override var timestamp: Long,
        override var comment: String = "",
        var diary: String
): DataItem()


@Database(entities = arrayOf(Weight::class, Diary::class, BloodPressure::class) , version = 3 , exportSchema = true)
    abstract class MediLogDB : RoomDatabase() {

    abstract fun weightDao(): WeightDao
    abstract fun diaryDao(): DiaryDao
    abstract fun bloodPressureDao(): BloodPressureDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Create the new table
                database.execSQL("CREATE TABLE IF NOT EXISTS `weight_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `weight` REAL NOT NULL)")
                database.execSQL("INSERT INTO weight_new (_id, timestamp, comment, weight) SELECT _id, timestamp, comment, weight FROM weight;")
                database.execSQL("DROP TABLE weight;")// Change the table name to the correct one
                database.execSQL("ALTER TABLE weight_new RENAME TO weight;")

                database.execSQL("CREATE TABLE IF NOT EXISTS `diary_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `diary` TEXT NOT NULL)")
                database.execSQL("INSERT INTO diary_new (_id, timestamp, comment, diary) SELECT _id, timestamp, '', diary FROM diary;")
                database.execSQL("DROP TABLE diary;")// Change the table name to the correct one
                database.execSQL("ALTER TABLE diary_new RENAME TO diary;")

                database.execSQL("CREATE TABLE IF NOT EXISTS `bp_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `sys` INTEGER NOT NULL, `dia` INTEGER NOT NULL, `pulse` INTEGER NOT NULL)")
                database.execSQL("INSERT INTO bp_new (_id, timestamp, comment, sys, dia, pulse) SELECT _id, timestamp, comment, systolic, diatolic, pulse FROM bloodpressure")
                database.execSQL("DROP TABLE bloodpressure")// Change the table name to the correct one
                database.execSQL("ALTER TABLE bp_new RENAME TO bloodpressure;")
            }
        }

        private val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Add the index
                database.execSQL("CREATE INDEX index_weight_timestamp on weight(timestamp)")
                database.execSQL("CREATE INDEX index_bloodpressure_timestamp on bloodpressure(timestamp)")
                database.execSQL("CREATE INDEX index_diary_timestamp on diary(timestamp)")
            }
        }

        @Volatile
        private var INSTANCE: MediLogDB? = null

        fun getDatabase(
                context: Context,
                scope: CoroutineScope
        ) : MediLogDB {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

            val dbName = "MediLogDatabase"
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance

            // Check if DB is already encrypted
            SQLiteDatabase.loadLibs(context)
            val dbFile = context.getDatabasePath(dbName)
            val state = getDatabaseState(dbFile)

            synchronized(this) {
                // No encryption, open DB and be done
                if (state == SQLCipherUtils.State.UNENCRYPTED) {
                    val instance = Room.databaseBuilder(
                            context.applicationContext,
                            MediLogDB::class.java,
                            dbName)
                            //                       .fallbackToDestructiveMigration()
                            .addMigrations(MIGRATION_1_2)
                            .addMigrations(MIGRATION_2_3)
                            .build()
                    INSTANCE = instance
                    val editor = sharedPref.edit()
                    editor.putBoolean("encryptedDB",false)
                    editor.apply()
                    return instance
                }

                // Retrieve pin, if none is available create a new one
                val databasePin: String? = sharedPref.getString("databasePin", UUID.randomUUID().toString())

                val passphrase: ByteArray = SQLiteDatabase.getBytes(databasePin?.toCharArray())
                val factory = SupportFactory(passphrase)
                val instance2 = Room.databaseBuilder(
                            context.applicationContext,
                            MediLogDB::class.java,
                            dbName)
                            .openHelperFactory(factory)
                            .addMigrations(MIGRATION_1_2)
                            .addMigrations(MIGRATION_2_3)
                            .build()
                INSTANCE = instance2

                val editor = sharedPref.edit()
                editor.putBoolean("encryptedDB",true)
                editor.putString("databasePin",databasePin)
                editor.apply()
                return instance2
                }
        }
    }
}
