package com.zell_mbc.medilog

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.base.DataViewModel
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.filterselector.*
import java.text.DateFormat
import java.util.*


class FilterFragment: Fragment() {
    var startDBCal = Calendar.getInstance()
    var endDBCal = Calendar.getInstance()

    val START = 0
    val END = 1

    var filterStart = 0L
    var filterEnd = 0L
    val filterStartCal = Calendar.getInstance()
    val filterEndCal = Calendar.getInstance()

    lateinit var viewModel: DataViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.filterselector, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val activeTab = sharedPref.getString("ACTIVETAB", "")
        when (activeTab) {
            getString(R.string.tab_weight)         -> viewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
            getString(R.string.tab_bloodpressure) -> viewModel = ViewModelProvider(this).get(BloodPressureViewModel::class.java)
            getString(R.string.tab_diary)          -> viewModel = ViewModelProvider(this).get(DiaryViewModel::class.java)
        }
        filterStart = sharedPref.getLong(viewModel.filterStartPref, 0L)
        filterEnd = sharedPref.getLong(viewModel.filterEndPref, 0L)
        dataTab.text = activeTab

        viewModel.init()


        filterStartCal.timeInMillis = filterStart
        if (filterEnd != 0L) filterEndCal.timeInMillis = filterEnd

        if (!filterStart.equals(0L)) {
            btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterStart)
        }

        if (!filterEnd.equals(0L)) {
            btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterEnd)
        }

        // create an OnDateSetListener
        val startDateListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                filterStartCal.set(Calendar.YEAR, year)
                filterStartCal.set(Calendar.MONTH, monthOfYear)
                filterStartCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateStartDate()
            }
        }

        val endDateListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                filterEndCal.set(Calendar.YEAR, year)
                filterEndCal.set(Calendar.MONTH, monthOfYear)
                filterEndCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateEndDate()
            }
        }

        //##############
        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        btStartDate.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                if (filterStartCal.timeInMillis == 0L) filterStartCal.setTimeInMillis(viewModel.getItems("ASC")[0].timestamp)
                DatePickerDialog(requireContext(),
                        startDateListener,
                        // set DatePickerDialog to point to the start of DB or today's date when it loads up
                        filterStartCal.get(Calendar.YEAR),
                        filterStartCal.get(Calendar.MONTH),
                        filterStartCal.get(Calendar.DAY_OF_MONTH)).show()
            } })

        btEndDate.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(requireContext(),
                        endDateListener,
                        // set DatePickerDialog to point to today's date when it loads up
                        filterEndCal.get(Calendar.YEAR),
                        filterEndCal.get(Calendar.MONTH),
                        filterEndCal.get(Calendar.DAY_OF_MONTH)).show()
            } })

        btDeleteFilterStart.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                btStartDate.setText("")
                filterStart = 0
                filterStartCal.timeInMillis = 0

                writeValue(START, filterStart)
            } })


        btDeleteFilterEnd.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                btEndDate.setText("")
                filterEndCal.timeInMillis = Calendar.getInstance().timeInMillis
                filterEnd = 0

                writeValue(END, filterEnd)
            } })

    }

    private fun updateStartDate() {
        val startFilter = filterStartCal.getTime()
        val endFilter = filterEndCal.getTime()

        if (startFilter < endFilter) {
            btStartDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(startFilter))
            filterStart = filterStartCal.timeInMillis
        } else {
            Snackbar.make(requireView(), getString(R.string.invalidFilterStartDate), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            filterStartCal.setTimeInMillis(viewModel.getItems("ASC")[0].timestamp)
        }
        writeValue(START, filterStart)
    }

    private fun updateEndDate() {
        val startFilter = filterStartCal.getTime()
        val endFilter = filterEndCal.getTime()

        if (endFilter > startFilter) {
                btEndDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(endFilter))
                filterEnd = filterEndCal.timeInMillis
            }
            else {
                Snackbar.make(requireView(), getString(R.string.invalidFilterEndDate), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
        writeValue(END, filterEnd)
    }

    fun writeValue(type: Int, value: Long) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPref.edit()
        if (type == START) editor.putLong(viewModel.filterStartPref, value)
        else               editor.putLong(viewModel.filterEndPref, value)
        editor.apply()
        viewModel.getFilter() // Make sure Values are updated
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
    }

}