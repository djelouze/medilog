package com.zell_mbc.medilog

import androidx.lifecycle.LiveData
import androidx.room.*


interface DataDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg obj: T)

    @Delete()
    suspend fun delete(vararg obj: T)

    @Update()
    suspend fun update(vararg obj: T)

}


@Dao
abstract class BloodPressureDao: DataDao<BloodPressure> {

    @Query("SELECT MAX(sys) FROM bloodpressure")
    abstract fun getMaxSys(): Int

    @Query("SELECT MAX(dia) FROM bloodpressure")
    abstract fun getMaxDia(): Int

    @Query("SELECT MIN(sys) FROM bloodpressure")
    abstract fun getMinSys(): Int

    @Query("SELECT MIN(dia) FROM bloodpressure")
    abstract fun getMinDia(): Int

    @Query("DELETE from bloodpressure where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from bloodpressure where comment = :commentValue")
    abstract suspend fun delete(commentValue: String)

    @Query("SELECT COUNT(*) FROM bloodpressure")
    abstract fun getSizeUnfiltered(): Int

    @Query("SELECT COUNT(*) FROM bloodpressure WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeFiltered(start: Long, end: Long): Int

    @Query("SELECT * from bloodpressure where _id = :id")
    abstract fun getItem(id: Int): BloodPressure

    @Query("SELECT * from bloodpressure where comment = :commentValue")
    abstract fun getItem(commentValue: String): BloodPressure

    // Recycler view
    @Query("SELECT * from bloodpressure ORDER BY timestamp DESC")
    abstract fun getBloodPressureItems(): LiveData<List<BloodPressure>>

    // Recycler view
    @Query("SELECT * from bloodpressure WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getBloodPressureItemsFiltered(start: Long, end: Long): LiveData<List<BloodPressure>>

    // Chart fragment
    @Query("SELECT * from bloodpressure ORDER BY timestamp ASC")
    abstract fun getBloodPressureItemsASC(): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getBloodPressureItemsASCFiltered(start: Long, end: Long): List<BloodPressure>

    @Query("SELECT * from bloodpressure ORDER BY timestamp DESC")
    abstract fun getBloodPressureItemsDESC(): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getBloodPressureItemsDESCFiltered(start: Long, end: Long): List<BloodPressure>

    @Query("DELETE FROM bloodpressure")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM bloodpressure WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)
}


@Dao
abstract class DiaryDao: DataDao<Diary> {

    @Query("DELETE from diary where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from diary where diary = :diaryValue")
    abstract suspend fun delete(diaryValue: String)

    @Query("SELECT COUNT(*) FROM diary")
    abstract fun getSizeUnfiltered(): Int

    @Query("SELECT COUNT(*) FROM diary WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeFiltered(start: Long, end: Long): Int

    @Query("SELECT * from diary where _id = :id")
    abstract fun getItem(id: Int): Diary

    @Query("SELECT * FROM diary WHERE diary = :diaryValue")
    abstract fun getItem(diaryValue: String): Diary

    // Recycler view
    @Query("SELECT * from diary ORDER BY timestamp DESC")
    abstract fun getDiaryItems(): LiveData<List<Diary>>

    // Recycler view
    @Query("SELECT * from diary WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getDiaryItemsFiltered(start: Long, end: Long): LiveData<List<Diary>>

    // Chart fragment
    @Query("SELECT * from diary ORDER BY timestamp ASC")
    abstract fun getDiaryItemsASC(): List<Diary>

    @Query("SELECT * from diary WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp  ASC")
    abstract fun getDiaryItemsASCFiltered(start: Long, end: Long): List<Diary>

    @Query("SELECT * from diary ORDER BY timestamp DESC")
    abstract fun getDiaryItemsDESC(): List<Diary>

    @Query("SELECT * from diary WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp  DESC")
    abstract fun getDiaryItemsDESCFiltered(start: Long, end: Long): List<Diary>

    @Query("DELETE FROM diary")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM diary WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)
}


@Dao
abstract class WeightDao: DataDao<Weight> {

    @Query("DELETE from weight where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from weight where weight = :weightValue")
    abstract suspend fun delete(weightValue: Float)

    @Query("SELECT MIN(weight) FROM weight")
    abstract fun getMinWeight(): Float

    @Query("SELECT MAX(weight) FROM weight")
    abstract fun getMaxWeight(): Float

    @Query("SELECT COUNT(*) FROM weight")
    abstract fun getSizeUnfiltered(): Int

    @Query("SELECT COUNT(*) FROM weight WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeFiltered(start: Long, end: Long): Int

    @Query("SELECT * from weight where _id = :id")
    abstract fun getItem(id: Int): Weight

    @Query("SELECT * FROM weight WHERE weight = :weightValue")
    abstract fun getItem(weightValue: Float): Weight

            // Recycler view
    @Query("SELECT * from weight ORDER BY timestamp DESC")
    abstract fun getWeightItems(): LiveData<List<Weight>>

    // Recycler view
    @Query("SELECT * from weight WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getWeightItemsFiltered(start: Long, end: Long): LiveData<List<Weight>>

    // Chart fragment
    @Query("SELECT * from weight ORDER BY timestamp ASC")
    abstract fun getWeightItemsASC(): List<Weight>

    @Query("SELECT * from weight WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getWeightItemsASCFiltered(start: Long, end: Long): List<Weight>

    @Query("SELECT * from weight ORDER BY timestamp DESC")
    abstract fun getWeightItemsDESC(): List<Weight>

    @Query("SELECT * from weight WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getWeightItemsDESCFiltered(start: Long, end: Long): List<Weight>

    @Query("DELETE FROM weight")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM weight WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)
}
