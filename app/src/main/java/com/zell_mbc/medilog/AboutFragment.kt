package com.zell_mbc.medilog

import android.os.Bundle
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.activity_about.*

class AboutFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))

        // Change application icon
        if (colourStyle == this.getString(R.string.green)) {
            iv_logo.setImageResource(R.mipmap.ic_launcher_green)
        }
        if (colourStyle == this.getString(R.string.blue)) {
            iv_logo.setImageResource(R.mipmap.ic_launcher_blue)
        }
        if (colourStyle == this.getString(R.string.red)) {
            iv_logo.setImageResource(R.mipmap.ic_launcher_red)
        }
        val versionCode = "Build " + BuildConfig.VERSION_CODE
        val versionName = "Version " + BuildConfig.VERSION_NAME

        tv_version.text = versionName
        tv_build.text = versionCode

        Linkify.addLinks(tv_email, Linkify.ALL)
        Linkify.addLinks(tv_mediLogLicenseUrl, Linkify.ALL)
        Linkify.addLinks(tv_androidPlotUrl, Linkify.ALL)
        Linkify.addLinks(tv_mediLogUrl, Linkify.ALL)
        Linkify.addLinks(tv_preferenceFixUrl, Linkify.ALL)
        Linkify.addLinks(tv_zip4j, Linkify.ALL)

        // Statistics
        val c = context
        if (c == null) {
            Log.d("--------------- Debug", " Chart Empty Context")
            return
        }
        val weightUnit = sharedPref.getString(SettingsActivity.KEY_PREF_WEIGHTUNIT, "kg")
        val showBloodPressureTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureTab, true)
        val showWeightTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWeightTab, true)
        val showDiaryTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showDiaryTab, true)

        if (sharedPref.getBoolean("encryptedDB", false)) tv_encryptedDB.text = getString(R.string.databaseEncryptionOn)
        else  tv_encryptedDB.text = getString(R.string.databaseEncryptionOff)

        if (sharedPref.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) tv_accessProtection.text = getString(R.string.biometricProtectionOn)
        else  tv_accessProtection.text = getString(R.string.biometricProtectionOff)

        val ss = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD,"")
        if (ss != null) {
            if (ss.length > 0) tv_backupProtection.text = getString(R.string.protectedBackupOn)
            else  tv_backupProtection.text = getString(R.string.protectedBackupOff)
        }

        var s: String
        if (showWeightTab) {
            val weightViewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
            weightViewModel.init()
            s = getString(R.string.weight) + ": " + weightViewModel.getSizeUnfiltered() + " " + getString(R.string.entries) + ", min " + weightViewModel.getMin() + " " + weightUnit + ", max " + weightViewModel.getMax() + " " + weightUnit
            tv_weightStats.text = s
        }
        if (showBloodPressureTab) {
            val bloodPressureViewModel = ViewModelProvider(this).get(BloodPressureViewModel::class.java)
            bloodPressureViewModel.init()
            s = getString(R.string.bloodPressure) + ": " + bloodPressureViewModel.getSizeUnfiltered() + " " + getString(R.string.entries) + ", " + " " + getString(R.string.systolic) + " min " + bloodPressureViewModel.getMinSys() + ", max " + bloodPressureViewModel.getMaxSys()
            tv_bloodPressureStats.text = s
            s = " " + getString(R.string.diastolic) +" min " + bloodPressureViewModel.getMinDia() + ", max " + bloodPressureViewModel.getMaxDia()
            tv_bloodPressureStats2.text = s
        }
        if (showDiaryTab) {
            val diaryViewModel = ViewModelProvider(this).get(DiaryViewModel::class.java)
            diaryViewModel.init()
            s = getString(R.string.diary) + ": " + diaryViewModel.getSizeUnfiltered() + " " + " " + getString(R.string.entries)
            tv_diaryStats.text = s
        }

        val dbName = "MediLogDatabase"
        val f = context?.getDatabasePath(dbName)
        val dbSize = f?.length()
        if (dbSize != null) {
            s = getString(R.string.databaseSize) + " " + (dbSize/1024).toString() + " kb"
            tv_dbSize.text = s
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
    }

}