package com.zell_mbc.medilog.weight

import androidx.lifecycle.LiveData
import com.zell_mbc.medilog.base.DataRepository
import com.zell_mbc.medilog.Weight
import com.zell_mbc.medilog.WeightDao


class WeightRepository(private val dao: WeightDao, private val filterStart: Long, private val filterEnd: Long): DataRepository() {
        // Room executes all queries on a separate thread.
        // Observed LiveData will notify the observer when the data has changed.
 //       val weightItems: LiveData<List<Weight>> = weightDao.getWeightItems()

        // Need to get access to parent filterEnd!!!
        val items: LiveData<List<Weight>> = dao.getWeightItemsFiltered(filterStart,filterEnd)

        override fun getItems(order: String): List<Weight> {
                if (order.equals("ASC")) return dao.getWeightItemsASC()
                else                    return dao.getWeightItemsDESC()
        }

        override fun getItemsFiltered(order: String, filterStart: Long, filterEnd: Long): List<Weight> {
                if (order.equals("ASC")) return dao.getWeightItemsASCFiltered(filterStart, filterEnd)
                else return dao.getWeightItemsDESCFiltered(filterStart, filterEnd)
        }

        override fun getSizeUnfiltered(): Int { return dao.getSizeUnfiltered() }
        override fun getSizeFiltered(filterStart: Long, filterEnd: Long): Int { return dao.getSizeFiltered(filterStart, filterEnd) }

        override fun getItem(index: Int): Weight = dao.getItem(index)
        fun getItem(weightValue: Float): Weight = dao.getItem(weightValue)

        fun getMin(): Float = dao.getMinWeight()
        fun getMax(): Float = dao.getMaxWeight()

        suspend fun insert(weight: Weight) { dao.insert(weight) }
        suspend fun update(weight: Weight) { dao.update(weight) }
        suspend fun delete(weight: Weight) { dao.delete(weight) }
        suspend fun delete(weightValue: Float) { dao.delete(weightValue) }

        override suspend fun delete(id: Int) { dao.delete(id) }
        override suspend fun deleteAll() { dao.deleteAll() }
        override suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) { dao.deleteAllFiltered(filterStart, filterEnd) }
        }