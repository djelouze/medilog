package com.zell_mbc.medilog.weight

import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.androidplot.util.AttrUtils.setColor
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import java.text.*
import java.util.*
import kotlin.math.roundToInt

// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class WeightChartFragment : Fragment() {
    var weightTreshold = ArrayList<Float>()
    var weights = ArrayList<Float>()
    var linearTrend = ArrayList<Float>()
    var movingAverage = ArrayList<Float>()
    var period = 5 // Minimum value = 2


    fun calculateMovingAverage() {
        val sample = Array<Float>(period,{_-> 0f}) // Create array of float, with all values set to 0
        val n = weights.size
        var sma: Float

        // the first n values in the sma will be off -> set them to the first weight value
        for (i in 0..period-1) {
            sample[i] = weights[0]
        }

        for (i in 0..n-1) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period-1] = weights[i]

            sma = 0f
            for (ii: Int in 0..period-1) {
                sma = sma + sample[ii]
            }
            sma = sma / period
            movingAverage.add(sma)
        }
    }

    fun calculateLinearTrendLine() {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        var a = 0f
        val b: Float
        var b1 = 0
        var b2 = 0f
        var c = 0
        val d: Float
        val e: Float
        val f: Float
        val g: Float
        val m: Float
        val n = weights.size
        for (i in 1..n) {
            a = a + i * weights[i - 1]
            b1 = b1 + i
            b2 = b2 + weights[i - 1]
            c = c + i * i
        }
        a = a * n
        b = b1 * b2
        c = n * c
        d = b1 * b1.toFloat()
        m = (a - b) / (c - d)
        e = b2
        f = m * b1
        g = (e - f) / n
        var value: Float
        for (i in 1..n) {
            value = m * i + g
            linearTrend.add(value)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.weight_chart, container, false)
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val c = context
        if (c == null) {
            Log.d("--------------- Debug", "Empty Context")
            return
        }

        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0f
        var wMin = 1000f

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(c)

        val daySteppingMode = sharedPref.getBoolean(SettingsActivity.KEY_PREF_weightDayStepping, false)
        val barChart = sharedPref.getBoolean(SettingsActivity.KEY_PREF_weightBarChart, false)
        var sTmp: String

        var fTmp: Float
        val simpleDate = SimpleDateFormat("MM-dd")
        val lastDate = Calendar.getInstance()
        var currentDate: Date?

        val viewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
        viewModel.init()
        val items = viewModel.getItems("ASC")
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            // Chart stepping by day
            if (daySteppingMode) {
                // Fill gap
                currentDate = try {
                    simpleDate.parse(sTmp)
                } catch (e: ParseException) {
                    continue
                }

                /*currentDate.
                Log.d("--------------- Debug", "currentDate: $currentDate")
                Log.d("--------------- Debug", "lastDate: " + lastDate.time)
                Log.d("--------------- Debug", "Compare: " + currentDate.compareTo(lastDate.time)) */
                while (labels.size > 0 && currentDate.compareTo(lastDate.time) > 0) {
                    sTmp = simpleDate.format(lastDate.time)
                    labels.add(sTmp)
                    Log.d("--------------- Debug", "GapDate: $sTmp")
                    weights.add(0f)
                    lastDate.add(Calendar.DAY_OF_MONTH, 1)
                }
                if (currentDate != null) lastDate.time = currentDate
            }
            labels.add(sTmp)
            weights.add(wi.weight)

            // Keep min and max values
            fTmp = wi.weight
            if (fTmp > wMax) {
                wMax = fTmp
            }
            if (fTmp < wMin) {
                wMin = fTmp
            }
        }
        if (weights.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val treshold = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWTHRESHOLDS, false)
        val s = sharedPref.getString(SettingsActivity.KEY_PREF_weightThreshold, "80")
        val weightThreshold = Integer.valueOf(s!!)
        if (treshold) {
            for (item in weights) {
                weightTreshold.add(weightThreshold.toFloat())
            }
        }

        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.weightPlot)
        val showGrid = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWGRID, true)
        if (!showGrid) {
            plot.getGraph().setDomainGridLinePaint(null)
            plot.getGraph().setRangeGridLinePaint(null)
        }
        else {
            plot.getGraph().domainGridLinePaint = Paint(Color.GRAY)
            plot.getGraph().rangeGridLinePaint = Paint(Color.GRAY) // Horizontal lines
        }

        val isLegendVisible = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWLEGEND, true)
        plot.legend.isVisible = isLegendVisible

        val series1: XYSeries = SimpleXYSeries(weights, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.weight))
        val series2: XYSeries = SimpleXYSeries(weightTreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.threshold))

        // Set boundaries to 10th at -20 and + 20 of min and max value
        val wMinBoundary = wMin.roundToInt() - 10
        val wMaxBoundary = wMax.roundToInt() + 10
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary/10*10) // Set to a 10er value

/*        Log.d("--------------- Debug", "wMin: $wMin")
        Log.d("--------------- Debug", "wMax: $wMax")
        Log.d("--------------- Debug", "wMinBoundary: $wMinBoundary")
        Log.d("--------------- Debug", "wMaxBoundary: $wMaxBoundary")
*/

        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 5.0)
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("####") // + weightUnit));  // Set integer y-Axis label

        // Line charts don't work without additional effort with date gaps, hence we switch to bar charts
        // Bar chart
        if (barChart) {
            val series1Format = BarFormatter(Color.BLUE, Color.BLUE)
            plot.addSeries(series1, series1Format)
        } else {
            val series1Format = LineAndPointFormatter(Color.BLUE, null, null, null)
            if (treshold) {
                val series1FormatLight = LineAndPointFormatter(Color.parseColor("#ADD8E6"), null, null, null)
                plot.addSeries(series2, series1FormatLight)
            }
            plot.addSeries(series1, series1Format)
        }

        // Trendlines
        val weightLinarTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_LINEAR_TRENDLINE, false)
        val weightMovingAverageTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE, false)

        if (weightLinarTrendLine) {
            calculateLinearTrendLine()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            val trendLine: XYSeries = SimpleXYSeries(linearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Linear Trend")

            trendFormat.isLegendIconEnabled = false;
            plot.addSeries(trendLine, trendFormat)
        }

        if (weightMovingAverageTrendLine) {
            period = sharedPref.getString(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE, "5")!!.toInt()
            calculateMovingAverage()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            trendFormat.isLegendIconEnabled = false;
            val simpleMovingAverage: XYSeries = SimpleXYSeries(movingAverage, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Moving Average")
            plot.addSeries(simpleMovingAverage, trendFormat)
        }

        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = Math.round((obj as Number).toFloat())
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}