package com.zell_mbc.medilog.weight

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.Weight
import java.text.DateFormat
import java.text.SimpleDateFormat

class WeightListAdapter internal constructor(context: Context) : RecyclerView.Adapter<WeightListAdapter.WeightViewHolder>() {
        private val inflater: LayoutInflater = LayoutInflater.from(context)
        private var clickListener: ItemClickListener? = null

        private var items = emptyList<Weight>() // Cached copy of weights, no ide why I need it

        private val textSize: Float
        private var weightUnit = ""
        private var dateFormat: DateFormat
        private var timeFormat: DateFormat
        private val highlightValues: Boolean
        private val weightThreshold: Int

    inner class WeightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val dateItem: TextView = itemView.findViewById(R.id.tvDateItem)
        val weightItem: TextView = itemView.findViewById(R.id.tvWeightItem)
        val commentItem: TextView = itemView.findViewById(R.id.tvCommentItem)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            dateItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            weightItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            commentItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeightViewHolder {
            val itemView = inflater.inflate(R.layout.weightview_row, parent, false)
            return WeightViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: WeightViewHolder, position: Int) {
            val orgColor = holder.dateItem.textColors.defaultColor

            val current = items[position]
//            holder.dateItem.text = SimpleDateFormat(format).format(current.timestamp)
//            holder.dateItem.text = DateFormat.getDateInstance(DateFormat.SHORT).format(current.timestamp) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(current.timestamp)
            holder.dateItem.text = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)

            val f: Float = current.weight
            val s = f.toString() + weightUnit
            if (highlightValues) {
                if (f >= weightThreshold) {
 //                   Log.d("WeightListAdapter: ", "Color weightThreshold " + weightThreshold + " f: " + f)
                    holder.weightItem.setTextColor(Color.rgb(255, 0, 0))
                    holder.weightItem.text = s
                } else {
                    holder.weightItem.setTextColor(orgColor)
                    holder.weightItem.text = s
//                    Log.d("WeightListAdapter: ", "noColor weightThreshold " + weightThreshold + " f: " + f)
                }
            }
            else holder.weightItem.text = s

            holder.commentItem.text = current.comment
        }

        internal fun setItems(items: List<Weight>) {
            this.items = items
            notifyDataSetChanged()
        }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        weightUnit = " " + sharedPref.getString(SettingsActivity.KEY_PREF_WEIGHTUNIT, weightUnit)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

        textSize = java.lang.Float.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, "15")!!)
        highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        weightThreshold = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_weightThreshold, "80")!!)
    }
}
