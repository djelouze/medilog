package com.zell_mbc.medilog.weight

import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.DataViewModel
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.IOException
import java.lang.StringBuilder
import java.text.DateFormat
import java.text.ParseException
import java.util.*

class WeightViewModel(application: Application): DataViewModel(application) {
    override val fileName = "Weight.csv"
    override var itemName = app.getString(R.string.weight)
    override val filterStartPref = "WEIGHTFILTERSTART"
    override val filterEndPref = "WEIGHTFILTEREND"

    override lateinit var repository: WeightRepository
    lateinit var dao: WeightDao
    lateinit var items: LiveData<List<Weight>>

    override fun init() {
        super.init()

        dao = MediLogDB.getDatabase(app, viewModelScope).weightDao()
        repository = WeightRepository(dao, filterStart, filterEnd)
        items = repository.items
    }

    fun insert(weight: Weight) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(weight)
    }

    fun update(weight: Weight) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(weight)
    }


   fun delete(weightValue: Float) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(weightValue)
    }

    override fun getItem(id: Int): Weight? {
        var item: Weight? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun getItem(weightValue: Float): Weight? {
        var item: Weight? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(weightValue) }
            j.join() }
        return item
    }


    override fun getItems(order: String): List<Weight> {
        getFilter()
        lateinit var items: List<Weight>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            if (filterActive) items = repository.getItemsFiltered(order, filterStart, filterEnd)
            else items = repository.getItems(order) }
            j.join() }
        return items
    }
/*
    override fun getSize(): Int {
        getFilter()
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            if (filterActive) i = repository.getSizeFiltered(filterStart, filterEnd)
            else i = repository.getSize() }
            j.join() }
        return i
    }
*/
    fun getMax(): Float{
        var f = 0f
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            f = repository.getMax() }
            j.join() }
        return f
    }

    fun getMin(): Float{
        var f = 0f
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            f = repository.getMin() }
            j.join() }
        return f
    }

// --------

    fun csvToItem(w: Weight, csv: String): Int {
        val data = csv.split(separator!!.toRegex()).toTypedArray()

        // At a minimum 2 values need to be present, comment is optional
        if (data.size < 2) {
            return -1
        }
        try {
            val date: Date? = csvPattern.parse(data[0].trim())
            if (date != null) w.timestamp = date.time
        } catch (e: ParseException) {
            return -2
        }
        try {
            w.weight = data[1].trim().toFloat()
        } catch (e: NumberFormatException) {
            return -3
        }

        // Comment not always available
        w.comment = if (data.size == 3) {
            data[2]
        } else {
            ""
        }
        return 0
    }


    override suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int {
        val tmpItems = ArrayList<Weight>()
        if (reader == null) {
            return 0
        }
        try {
            var tmpLine = ""
            var showError = true
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                // Quick line format check
                val item = Weight(0, 0,"", 0f) // Start with empty item
                // At a minimum 2 values need to be present, comment is optional
                val err = csvToItem(item, line!!)
                lineNo++
                if (err < 0) {
                    if (showError) {
                        Handler(Looper.getMainLooper()).post {
                            Toast.makeText(app, "Error " + err + " importing line " + lineNo + ", '" + tmpLine + "'", Toast.LENGTH_LONG).show()
                        }
                        showError = false // Show only first error to avoid pushing hundreds of similar errors
                    }
                    continue
                }
                tmpItems.add(item)
            }
        }
        catch (e: IOException) {
            return -1
        }

        return transferItems(tmpItems, replaceData)
    }


    suspend fun transferItems(tmpItems: ArrayList<Weight>, replaceData: Boolean): Int {
        val records = tmpItems.size
        if (records == 0) return -2     // Delete only after a successful import

        // Delete only after a successful import
        if (replaceData) {
            deleteAllUnfiltered()
            // Wait until DB is empty
            var i = 5
            while (repository.getSizeUnfiltered() > 0 && i > 0) {
                //                   Log.d("SuspendedInsert", "Avoid race condition -> Wait 100ms")
                delay(100L)
                Log.d("--------------- Debug", "Delay:" + i--)
                if (i == 0) {
                    // If after 10 rounds the DB is not empty something odd is going on
                    Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Error! Unable to delete all old records. ", Toast.LENGTH_LONG).show() }
                }
            }
        }
        for (item in tmpItems) {
            val job = insert(item)
            job.join()
        }

        return records
    }

    // CSV header
    override fun csvData(filtered: Boolean): String {
        val sb = StringBuilder()

        // CSV header
        sb.append(app.getString(R.string.date) + separator + app.getString(R.string.weight) + System.getProperty("line.separator"))

        // Data
        val weights = getItems("DESC")
        for (item: Weight in weights) {
            sb.append(csvPattern.format(item.timestamp) + separator + item.weight + separator + item.comment + System.getProperty("line.separator"))
        }
        return sb.toString()
    }


    override fun createPDF(): PdfDocument? {
        if (getSizeFiltered() == 0) {
            Toast.makeText(app, app.getString(R.string.weight) + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }
        val userName = sharedPref.getString(SettingsActivity.KEY_PREF_USER, "")
        val weightUnit = sharedPref.getString(SettingsActivity.KEY_PREF_WEIGHTUNIT, "kg")
        val highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        val weightThreshold = sharedPref.getString(SettingsActivity.KEY_PREF_weightThreshold, "80")!!.toInt()

        val document = PdfDocument()

//        int tab1 = 80;
        var pageNumber = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas
        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK

        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true

        val paintRed = Paint()
        paintRed.color = Color.RED

        // Black & White instead of colour
        val BW = true

        // -----------

        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas
        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        var headerText = app.getString(R.string.weightReportTitle)
        if (userName != null) headerText = headerText + " " + app.getString(R.string.forString) + " " + userName

        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        i = pdfDataTop
        // Print header in bold
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.weight) + " (" + weightUnit + ")", pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        /*
        if (true) {
            document.finishPage(page)
            return document
        }
*/
// Todo: Add comment field to pdf
        // ------------
        var left = pdfLeftBorder // Value changes based on column
        var activeColumn = 1
        val column2 = canvas.width / 2 + left

        val weights = getItems("ASC")
        for (wi in weights) {
            i = i + pdfLineSpacing
            if (i > pdfDataBottom) {
                // Second column
                if (activeColumn == 1) {
                    activeColumn = 2
                    i = pdfDataTop
                    // Column separator
                    pdfPaint.color = Color.DKGRAY
                    canvas.drawLine(column2.toFloat(), pdfHeaderBottom.toFloat(), column2.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                    pdfPaint.color = Color.BLACK
                    left = column2 + pdfLeftBorder
                    canvas.drawText(app.getString(R.string.date), left.toFloat(), i.toFloat(), pdfPaintHighlight)
                    canvas.drawText(app.getString(R.string.weight) + " (" + weightUnit + ")", (left + pdfDataTab).toFloat(), i.toFloat(), pdfPaintHighlight)
                } else {
                    document.finishPage(page)
                    pageNumber = pageNumber + 1
                    activeColumn = 1

                    // crate a A4 page description
                    pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
                    page = document.startPage(pageInfo)
                    canvas = page.canvas
                    left = pdfLeftBorder

                    canvas.drawText(headerText, left.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                    canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                    canvas.drawLine(left.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                    canvas.drawLine(left.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                    i = pdfDataTop
                    canvas.drawText(app.getString(R.string.date), left.toFloat(), i.toFloat(), pdfPaint)
                    canvas.drawText(app.getString(R.string.weight) + " (" + weightUnit + ")", (left + pdfDataTab).toFloat(), i.toFloat(), pdfPaintHighlight)
                    // ------------
                }
                i += pdfLineSpacing
            }
            canvas.drawText(toStringDate(wi.timestamp), left.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(toStringTime(wi.timestamp), left + pdfTimeTab.toFloat(), i.toFloat(), pdfPaint)

            if (highlightValues and (wi.weight > weightThreshold)) {
                if (BW) {
                    canvas.drawText(wi.weight.toString(), left + pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)
                } else { // Colour
                    canvas.drawText(wi.weight.toString(), left +pdfDataTab.toFloat(), i.toFloat(), paintRed)
                }
            }
            else {
                    canvas.drawText(wi.weight.toString(), left + pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
            }
        }
        // finish the page
        document.finishPage(page)
        return document
    }

}
