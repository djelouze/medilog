package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import android.util.Log
import androidx.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import java.util.*

class BloodPressureChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        when (colourStyle) {
            this.getString(R.string.green) -> theme.applyStyle(R.style.AppThemeGreenBar, true)
            this.getString(R.string.blue)  -> theme.applyStyle(R.style.AppThemeBlueBar, true)
            this.getString(R.string.red)   -> theme.applyStyle(R.style.AppThemeRedBar, true)
        }

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, BloodPressureChartFragment())
                .commit()
    }
}