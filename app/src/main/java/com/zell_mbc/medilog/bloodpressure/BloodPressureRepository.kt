package com.zell_mbc.medilog.bloodpressure

import androidx.lifecycle.LiveData
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.DataRepository

class BloodPressureRepository(private val dao: BloodPressureDao, private val filterStart: Long, private val filterEnd: Long): DataRepository() {
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val items: LiveData<List<BloodPressure>> = dao.getBloodPressureItemsFiltered(filterStart,filterEnd)

    override fun getItems(order: String): List<BloodPressure> {
        if (order.equals("ASC")) return dao.getBloodPressureItemsASC()
        else                     return dao.getBloodPressureItemsDESC()
    }

    override fun getItemsFiltered(order: String, start: Long, end: Long): List<BloodPressure> {
        if (order.equals("ASC")) return dao.getBloodPressureItemsASCFiltered(start, end)
        else                     return dao.getBloodPressureItemsDESCFiltered(start, end)
    }

    override fun getSizeUnfiltered(): Int { return dao.getSizeUnfiltered() }
    override fun getSizeFiltered(filterStart: Long, filterEnd: Long): Int { return dao.getSizeFiltered(filterStart, filterEnd) }
    override fun getItem(index: Int): BloodPressure = dao.getItem(index)
    fun getItem(commentValue: String): BloodPressure = dao.getItem(commentValue)

    fun getMaxSys(): Int = dao.getMaxSys()
    fun getMinSys(): Int = dao.getMinSys()
    fun getMaxDia(): Int = dao.getMaxDia()
    fun getMinDia(): Int = dao.getMinDia()

    suspend fun insert(bloodPressure: BloodPressure) { dao.insert(bloodPressure) }
    suspend fun update(bloodPressure: BloodPressure) { dao.update(bloodPressure) }
    suspend fun delete(bloodPressure: BloodPressure) { dao.delete(bloodPressure) }
    suspend fun delete(commentValue: String) { dao.delete(commentValue) }

    override suspend fun delete(id: Int) { dao.delete(id) }
    override suspend fun deleteAll() { dao.deleteAll() }
    override suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) { dao.deleteAllFiltered(filterStart, filterEnd) }
}