package com.zell_mbc.medilog.bloodpressure

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethod
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import kotlinx.android.synthetic.main.bloodpressureeditform.button_saveData
import kotlinx.android.synthetic.main.bloodpressureeditform.etComment
import kotlinx.android.synthetic.main.bloodpressureeditform.etDate
import kotlinx.android.synthetic.main.bloodpressureeditform.etDia
import kotlinx.android.synthetic.main.bloodpressureeditform.etPulse
import kotlinx.android.synthetic.main.bloodpressureeditform.etSys
import kotlinx.android.synthetic.main.bloodpressureeditform.etTime
import kotlinx.android.synthetic.main.weighteditform.*
import java.text.DateFormat
import java.util.*

class BloodPressureEditFragment : Fragment() {
    private val viewModel: BloodPressureViewModel by viewModels()
    var _id: Int = 0 // id is already taken by Fragment class
    private val tmpComment = "temporaryBloodPressureEntry-YouShouldNeverSeeThis"
    private val timestampCal = Calendar.getInstance()

    fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.length > 0) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) Snackbar.make(requireView(), errorMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()

        return valueInt
    }


    private fun saveItem() {
        val editItem = viewModel.getItem(_id)
        if (editItem == null) {
            Snackbar.make(requireView(), "Unknown error!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Savely convert string values
        var value = 0
        value = stringToInt(etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return
        editItem.sys = value

        value = stringToInt(etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return
        editItem.dia = value

        value = stringToInt(etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return
        editItem.pulse = value

        editItem.timestamp = timestampCal.timeInMillis
        editItem.comment = etComment.text.toString()

        viewModel.update(editItem)

        Snackbar.make(requireView(),getString(R.string.itemUpdated), Snackbar.LENGTH_LONG).setAction("Action", null).show()
        requireActivity().onBackPressed()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bloodpressureeditform, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = button_saveData as FloatingActionButton
        when (colourStyle) {
            this.getString(R.string.green) -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }

        viewModel.init()
        val editItem = viewModel.getItem(_id)

        if (editItem != null) {
            this.etSys.setText(editItem.sys.toString())
            this.etDia.setText(editItem.dia.toString())
            this.etPulse.setText(editItem.pulse.toString())

            // Check if we are really editing or if this is a new value
            if (editItem.comment == tmpComment) {
                this.etComment.setText("")
                this.etSys.setText("") // Make sure fields are empty to allow quick data entry
                this.etDia.setText("")
                this.etPulse.setText("")
            }
            else
                this.etComment.setText(editItem.comment)

            this.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
            this.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)
        }
        // Respond to click events
        saveButton.setOnClickListener { saveItem() }

        // ---------------------
        // Date/Time picker section
        if (editItem != null) {
            timestampCal.setTimeInMillis(editItem.timestamp)
        }

        // create an OnDateSetListener
        val dateListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                timestampCal.set(Calendar.YEAR, year)
                timestampCal.set(Calendar.MONTH, monthOfYear)
                timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                etDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis))
                etTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis))
            }
        }

        val timeListener = object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(view: TimePicker, hour: Int, minute: Int) {
                timestampCal.set(Calendar.HOUR, hour)
                timestampCal.set(Calendar.MINUTE, minute)

                etDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis))
                etTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis))
            }
        }

//        btDatePicker!!.setOnClickListener(object : View.OnClickListener {
        etDate.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(requireContext(),
                        dateListener,
                        timestampCal.get(Calendar.YEAR),
                        timestampCal.get(Calendar.MONTH),
                        timestampCal.get(Calendar.DAY_OF_MONTH)).show()
            } })

        etTime.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                TimePickerDialog(requireContext(),
                        timeListener,
                        timestampCal.get(Calendar.HOUR_OF_DAY),
                        timestampCal.get(Calendar.MINUTE),
                        android.text.format.DateFormat.is24HourFormat(requireContext())).show()
            } })

        // Make sure first field is highlighted and keyboard is open
        this.etSys.requestFocus()
    }

    fun newInstance(i: Int): BloodPressureEditFragment? {
        val f = BloodPressureEditFragment()
        f._id = i
        return f
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.delete(tmpComment)
    }
}