package com.zell_mbc.medilog.bloodpressure

import android.app.AlertDialog
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.*
import kotlinx.android.synthetic.main.bloodpressure_tab.*
import java.util.*
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.bloodpressure_tab.*
import java.text.DateFormat

class BloodPressureFragment : Fragment(), BloodPressureListAdapter.ItemClickListener {
    private lateinit var viewModel: BloodPressureViewModel
    private var adapter: BloodPressureListAdapter? = null
    private var quickEntry = true

    fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.length > 0) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) Snackbar.make(requireView(), errorMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()

        return valueInt
    }


    private fun addItem() {

        if (!quickEntry) {
            val tmpComment = "temporaryBloodPressureEntry-YouShouldNeverSeeThis"
            val tmpItem = BloodPressure(0, Date().time, tmpComment, 0,0,0)
            viewModel.insert(tmpItem)
            var newItem = viewModel.getItem(tmpComment)
            var turns = 100
            while (newItem == null && turns > 0) {
                Log.d("Wait for Insert to complete", "Turns $turns")
                turns--
                newItem = viewModel.getItem(tmpComment)
            }
            if (newItem != null) editItem(newItem._id)
            else Snackbar.make(requireView(), "No entry with tmp Comment found!"  , Snackbar.LENGTH_LONG).setAction("Action", null).show()

            return
        }

        // ###########################
        // Checks
        // ###########################
        // Savely convert string values
        var value: Int = stringToInt(etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return
        val iSys = value

        value = stringToInt(etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return
        val iDia = value

        value = stringToInt(etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return
        val iPulse = value

        val sC = etComment.text.toString()

        val item = BloodPressure(0, Date().time, sC, iSys, iDia, iPulse)

        viewModel.insert(item)
        if (viewModel.filterActive && (viewModel.filterEnd < item.timestamp)) Snackbar.make(requireView(), getString(R.string.filteredOut), 4000).setAction("Action", null).show()
 //       else Snackbar.make(requireView(), getString(R.string.word_new) + " " + viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_saved), Snackbar.LENGTH_LONG).setAction("Action", null).show()

        etSys.setText("")
        etDia.setText("")
        etPulse.setText("")
        etComment.setText("")
        etSys.bringToFront()
    }

    private fun editItem(index: Int) {
        Log.d("--------------- Debug", "Index: " + index)

        val intent = Intent(requireContext(), BloodPressureEditActivity::class.java)
        val extras = Bundle()
        val bp = viewModel.getItem(index)
        Log.d("--------------- Debug", "bp: " + bp)
        if (bp != null) {
            extras.putInt("ID", bp._id)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        // Remove from array
        Log.d("Debug BPFragment", "Delete: " + index)
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        Snackbar.make(requireView(), viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    fun notifyDataSetChanged() {
        adapter?.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("BloodPressureFragment onCreateView : ", "Start" + inflater + " parent " + container + " bundle " + savedInstanceState)
        //       super.onCreate(savedInstanceState)
        return inflater.inflate(R.layout.bloodpressure_tab, container, false)
    }

    override fun onStart() {
        Log.d("BloodPressureFragment onStart : ", "")
        super.onStart()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("BloodPressureFragment onViewCreated : ", "Start")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val addButton = button_addBloodPressure as FloatingActionButton
        quickEntry = sharedPref.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)

        when (colourStyle) {
            this.getString(R.string.green) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }

        // Hide quick entry fields if needed
        if (!quickEntry) {
            etSys.visibility = View.GONE
            etDia.visibility = View.GONE
            etPulse.visibility = View.GONE
            etComment.visibility = View.GONE
        }
        else {
            etSys.visibility = View.VISIBLE
            etDia.visibility = View.VISIBLE
            etPulse.visibility = View.VISIBLE
            etComment.visibility = View.VISIBLE
        }

        // set up the RecyclerView
        val layoutManager = LinearLayoutManager(requireContext())
        rvBloodPressureList.layoutManager = layoutManager

        adapter = BloodPressureListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
        viewModel.repository.items.observe(requireActivity(), Observer { bloodPressure -> bloodPressure?.let { adapter!!.setBloodPressures(it) } })

        adapter!!.setClickListener(this)
        rvBloodPressureList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(rvBloodPressureList.context, layoutManager.orientation)
        rvBloodPressureList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        addButton.setOnClickListener { addItem() }

        val showChartButton = button_showChart as FloatingActionButton
        showChartButton.setOnClickListener(View.OnClickListener {
            val c = context
            if (c == null) {
                Log.d("Debug OpenChart: ", "Empty Context")
                return@OnClickListener
            }
            if (viewModel.getSizeFiltered() < 2) {
                Snackbar.make(view, c.getString(R.string.notEnoughDataForChart), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return@OnClickListener
            }
            val intent = Intent(requireContext(), BloodPressureChartActivity::class.java)
            startActivity(intent)
        })
    }

    override fun onDestroy() {
        Log.d("Debug: BloodPressure Fragment", " Destroyed!")
        super.onDestroy()
    }

    override fun onItemClick(view: View?, position: Int) {
        val c = context
        if (c == null) {
            Log.d("Debug onViewCreated: ", "Empty Context")
            return
        }
        val alertDialogBuilder = AlertDialog.Builder(c)
        val item: BloodPressure? = viewModel.items.value!![position] //getBloodPressureItem(position)
        item ?: return
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        alertDialogBuilder.setTitle(getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
        // set dialog message
        alertDialogBuilder
                .setMessage(getString(R.string.whatDoYouWant))
                .setCancelable(false)
                .setPositiveButton("Delete") { dialog, _ ->
                    deleteItem(item._id)
                    dialog.cancel()
                }
                .setNeutralButton("Cancel") { dialog, _ -> dialog.cancel() }
                .setNegativeButton("Edit") { dialog, _ ->
                    editItem(item._id)
                    dialog.cancel()
                }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}