package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import android.widget.Toast
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity


class BloodPressureHelper(val context: Context) {
    var hyperGrade3Sys = 0
    var hyperGrade3Dia = 0
    var hyperGrade2Sys = 0
    var hyperGrade2Dia = 0
    var hyperGrade1Sys = 0
    var hyperGrade1Dia = 0

    val hyperGrade1 = 1
    val hyperGrade2 = 2
    val hyperGrade3 = 3

    init {
        // Check values
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        var sTmp = sharedPref.getString(SettingsActivity.KEY_PREF_hyperGrade1, context.getString(R.string.grade1Values))
        try {
            val grade = sTmp!!.split(",".toRegex()).toTypedArray()
            hyperGrade1Sys = grade[0].toInt()
            hyperGrade1Dia = grade[1].toInt()
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.grade1Error) + " " + sTmp + " , " + context.getString(R.string.gradeErrorEnd), Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
        sTmp = sharedPref.getString(SettingsActivity.KEY_PREF_hyperGrade2, context.getString(R.string.grade2Values))
        try {
            val grade = sTmp!!.split(",".toRegex()).toTypedArray()
            hyperGrade2Sys = grade[0].toInt()
            hyperGrade2Dia = grade[1].toInt()
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.grade2Error) + " " + sTmp + " , " + context.getString(R.string.gradeErrorEnd), Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
        sTmp = sharedPref.getString(SettingsActivity.KEY_PREF_hyperGrade3, context.getString(R.string.grade3Values))
        try {
            val grade = sTmp!!.split(",".toRegex()).toTypedArray()
            hyperGrade3Sys = grade[0].toInt()
            hyperGrade3Dia = grade[1].toInt()
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.grade3Error) + " " + sTmp + " , " + context.getString(R.string.gradeErrorEnd), Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
    }

    fun sysGrade(sys: Int): Int {
        if (sys >= hyperGrade3Sys) return hyperGrade3
        if (sys >= hyperGrade2Sys) return hyperGrade2
        if (sys >= hyperGrade1Sys) return hyperGrade1
        return 0
    }

    fun diaGrade(dia: Int): Int {
        if (dia >= hyperGrade3Dia) return hyperGrade3
        if (dia >= hyperGrade2Dia) return hyperGrade2
        if (dia >= hyperGrade1Dia) return hyperGrade1
        return 0
    }
}