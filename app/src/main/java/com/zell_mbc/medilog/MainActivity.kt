package com.zell_mbc.medilog

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.PromptInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getColor
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.zell_mbc.medilog.R.*
import com.zell_mbc.medilog.base.DataViewModel
import com.zell_mbc.medilog.bloodpressure.BloodPressureFragment
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryFragment
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.weight.WeightFragment
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.activity_main.*
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.*
import java.text.DateFormat.getDateInstance
import java.text.DateFormat.getTimeInstance
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    lateinit var mediLog: MediLog

    private val rcRESTORE = 9998
    private val rcBACKUP = 9999

//    lateval mainViewModel: ViewModel
    lateinit var weightViewModel: WeightViewModel
    lateinit var bloodPressureViewModel: BloodPressureViewModel
    lateinit var diaryViewModel: DiaryViewModel

    // Fragments
    private var bloodPressureFragment: BloodPressureFragment? = null
    private var weightFragment: WeightFragment? = null
    private var diaryFragment: DiaryFragment? = null

    // Initialize with negative value to clearly differentiate from tab values
    var bloodPressureFragment_tabID = -1
    var weightFragment_tabID = -1
    var diaryFragment_tabID = -1

    var hideMenus = false
    val mContext: Context = this
    var reader: BufferedReader? = null

    override fun onCreate(savedInstanceState: Bundle?) {

/*        if (DEVELOPER_MODE) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
       }
*/
        super.onCreate(savedInstanceState)
        Log.d("MainActivity", "onCreate")
        mediLog = applicationContext as MediLog
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val c = this
        if (!sharedPref.getBoolean("DISCLAIMERCONFIRMED", false)) {
            val alertDialog = AlertDialog.Builder(c).create()
            alertDialog.setTitle("Disclaimer")
            alertDialog.setMessage(getString(string.disclaimerText))
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialog, _ -> dialog.dismiss()
                val editor = sharedPref.edit()
                editor.putBoolean("DISCLAIMERCONFIRMED", true)
                editor.apply()
//                Log.d("MainActivity", " Disclaimer read")
            }
            alertDialog.show()
        }

        diaryViewModel = ViewModelProvider(this).get(DiaryViewModel::class.java)
        diaryViewModel.init()

        weightViewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
        weightViewModel.init()

        bloodPressureViewModel = ViewModelProvider(this).get(BloodPressureViewModel::class.java)
        bloodPressureViewModel.init()

        setTheme()
        setContentView(layout.activity_main)

        val showBloodPressureTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureTab, true)
        val showWeightTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWeightTab, true)
        val showDiaryTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showDiaryTab, true)
        val activeTab = sharedPref.getInt("activeTab", 0)

        // Initialize delimiter on first run based on locale
        var delimiter = sharedPref.getString(SettingsActivity.KEY_PREF_DELIMITER, "")
        if (delimiter.equals("")) {
            val currentLanguage = Locale.getDefault().displayLanguage
            if (currentLanguage.contains("Deutsch")) {
                // Central Europe
                delimiter = ";"
            }
            else delimiter = ","

            val editor = sharedPref.edit()
            editor.putString(SettingsActivity.KEY_PREF_DELIMITER, delimiter)
            editor.apply()
        }

        hideMenus = false

        val adapter = TabAdapter(supportFragmentManager)

        if (showWeightTab) {
            weightFragment = WeightFragment()
            adapter.addFragment(weightFragment!!, getString(string.tab_weight))
            weightFragment_tabID = adapter.count - 1
            }
           if (showBloodPressureTab) {
            bloodPressureFragment = BloodPressureFragment()
           if (bloodPressureFragment != null)
            adapter.addFragment(bloodPressureFragment!!, getString(string.tab_bloodpressure))
            bloodPressureFragment_tabID = adapter.count - 1
        }
        if (showDiaryTab) {
            diaryFragment = DiaryFragment()
            adapter.addFragment(diaryFragment!!, getString(string.tab_diary))
            diaryFragment_tabID = adapter.count - 1
        }
        if (!(showWeightTab || showBloodPressureTab || showDiaryTab)) {
            hideMenus = true
            Snackbar.make(findViewById(android.R.id.content), getString(string.noActiveTab), Snackbar.LENGTH_LONG).setAction("Action", null).show()
        }
        viewPager.setAdapter(adapter)
        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                invalidateOptionsMenu()
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        tabLayout.setupWithViewPager(viewPager)
        viewPager.setCurrentItem(activeTab)
        val toolbar = findViewById<Toolbar>(id.toolbar)
        setSupportActionBar(toolbar)
    }


    override fun onStart() {
        Log.d("MainActivity", "onStart")
        super.onStart()

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        // If false, set authenticated to true and be done with it
        if (!sharedPref.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) mediLog.setAuthenticated(true)
        else {
            // Check timer
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            val now = Calendar.getInstance().timeInMillis
            val threshold = sharedPref.getLong("FORCE_REAUTHENTICATION", 300000) // 5 minutes
            val then = sharedPref.getLong("STOPWATCH", 0L)
            val diff = now - then

            // First check if authentication is enabled
            // If yes, check if re-authentication needs to be forced = if more than threshold milliseconds passed after last onStop event was executed
            if (diff > threshold) {
                mediLog.setAuthenticated(false)
            }

            val biometricHelper = BiometricHelper(this)
            val canAuthenticate = biometricHelper.canAuthenticate(true)
            if (!mediLog.isAuthenticated() && canAuthenticate == 0) {
                // No idea why I need this
                val newExecutor: Executor = Executors.newSingleThreadExecutor()
                val activity: FragmentActivity = this
                val myBiometricPrompt = BiometricPrompt(activity, newExecutor, object : BiometricPrompt.AuthenticationCallback() {

                    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                        super.onAuthenticationError(errorCode, errString)
                        val snackbar = Snackbar
                                .make(findViewById(android.R.id.content), "$errString " + getString(string.retryActionText), 300000)
                                .setAction(getString(string.retryAction)) { onStart() }
                        snackbar.setActionTextColor(Color.RED)
                        snackbar.show()
                    }

                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                        super.onAuthenticationSucceeded(result)
                        mediLog.setAuthenticated(true)
                        runOnUiThread { viewPager?.visibility = View.VISIBLE }
                    }

                })
                runOnUiThread { viewPager?.visibility = View.INVISIBLE }
                val promptInfo = PromptInfo.Builder()
                        .setTitle(getString(string.app_name))
// ToDo: Disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
//                        .setDeviceCredentialAllowed(true)  // Allow to use pin as well
                        .setNegativeButtonText(getString(string.cancel))
                        .setSubtitle(getString(string.biometricLogin)) //                        .setDescription(getString(R.string.biometricLogin)
                        .setConfirmationRequired(false)
                        .build()
                myBiometricPrompt.authenticate(promptInfo)
            }
        }
    }
    override fun onRestart() {
        super.onRestart()
        // Check view model
        Log.d("MainActivity", "onRestart Authenticated = " + mediLog.isAuthenticated())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        var filterActive = false

        viewPager.currentItem = tabLayout.selectedTabPosition
        when (tabLayout.selectedTabPosition) {
            bloodPressureFragment_tabID -> filterActive = bloodPressureViewModel.filterActive
            weightFragment_tabID -> filterActive = weightViewModel.filterActive
            diaryFragment_tabID -> filterActive = diaryViewModel.filterActive
        }
//        Log.d("MainActivity", "onCreateOptionsMenu: filterstart " + filterStart + " filterEnd " + filterEnd)

        val filterIcon = menu.findItem(id.action_setFilter).icon
        filterIcon.mutate()
        if (filterActive) { filterIcon.setTint(getColor(this, color.colorActiveFilter)) }

        // Disable certain menus if no active Tab exists
        if (hideMenus) {
            menu.findItem(id.action_dataManagement).isVisible = false
            menu.findItem(id.action_send).isVisible = false
        }
        /*        else {
            menu.findItem(R.id.action_dataManagement).setVisible(true);
            menu.findItem(R.id.action_send).setVisible(false);
       }*/
        return true
    }

    // Menue
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val activeTab = tabLayout?.selectedTabPosition
        when (id) {
            R.id.action_setFilter -> {
                val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = sharedPref.edit()
                when (tabLayout.selectedTabPosition) {
                    weightFragment_tabID -> editor.putString("ACTIVETAB", getString(string.tab_weight))
                    bloodPressureFragment_tabID -> editor.putString("ACTIVETAB", getString(string.tab_bloodpressure))
                    diaryFragment_tabID -> editor.putString("ACTIVETAB", getString(string.tab_diary))
                }
                editor.apply()

                val intent = Intent(this, FilterActivity::class.java)
                startActivity(intent)
                return true
            }

            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }

            R.id.action_sendZIP -> {
                // Need to explicitly ask for permission once
                val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *PERMISSIONS)) {
                    ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                }

                //        Intent intent = new Intent(Intent.ACTION_VIEW);
                val intent = Intent(Intent.ACTION_SEND)
                var uri: Uri? = null
                intent.type = "application/zip"
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(string.sendFile))
                intent.putExtra(Intent.EXTRA_TEXT, getString(string.sendFile))

                when (activeTab) {
                    bloodPressureFragment_tabID -> {
                        uri = bloodPressureViewModel.getZIP(bloodPressureViewModel.createPDF())
                        intent.putExtra(Intent.EXTRA_STREAM, uri)
                    }
                    weightFragment_tabID -> {
                        uri = weightViewModel.getZIP(weightViewModel.createPDF())
                        intent.putExtra(Intent.EXTRA_STREAM, uri)
                    }
                    diaryFragment_tabID -> {
                        uri = diaryViewModel.getZIP(diaryViewModel.createPDF())
                        intent.putExtra(Intent.EXTRA_STREAM, uri)
                    }
                }

                if ((uri != null) && (intent.resolveActivity(packageManager) != null)) {
                    try {
                        startActivity(Intent.createChooser(intent, getString(string.sendFile)))
                    } catch (e: Exception) {
                        Snackbar.make(findViewById(android.R.id.content),getString(string.eShareError) + ": " + e.localizedMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                        Toast.makeText(this,getString(string.eShareError) + ": " + e.localizedMessage, Toast.LENGTH_LONG).show()
                        Log.e("Exception", getString(string.eShareError) + e.toString())
                    }
                }
                return true
            }
            R.id.action_sendReport -> {
                val intent = Intent(Intent.ACTION_VIEW)
                var uri: Uri? = null
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

                when(activeTab) {
                    bloodPressureFragment_tabID -> uri = bloodPressureViewModel.getPDF(bloodPressureViewModel.createPDF())
                    weightFragment_tabID -> uri = weightViewModel.getPDF(weightViewModel.createPDF())
                    diaryFragment_tabID -> uri = diaryViewModel.getPDF(diaryViewModel.createPDF())
                }

                if (uri != null) {
                    // Need to explicitly ask for permission once
                    val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    if (!hasPermissions(mContext, *PERMISSIONS)) {
                        ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                    }

                    intent.setDataAndType(uri, "application/*")
                    val pm = packageManager
                    if (intent.resolveActivity(pm) != null) {
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                            Snackbar.make(findViewById(android.R.id.content), getString(string.eShareError) + ": " + e.localizedMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                        }
                    }
                }
                return true
            }
            R.id.action_sendRawData -> {
                lateinit var vm: DataViewModel
                when (activeTab) {
                    weightFragment_tabID -> vm = weightViewModel
                    bloodPressureFragment_tabID -> vm = bloodPressureViewModel
                    diaryFragment_tabID -> vm = diaryViewModel
                }

                if (vm.getSizeFiltered() == 0) {
                    Snackbar.make(findViewById(android.R.id.content), vm.itemName + " " + getString(string.noDataToExport), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                    return false
                }

                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, vm.csvData(true))
                startActivity(Intent.createChooser(sendIntent, getString(string.word_send) + " " + vm.itemName + " " + getString(string.sendTo)))
                return true
            }
            R.id.action_deleteData -> {
                lateinit var vm: DataViewModel
                when (activeTab) {
                    weightFragment_tabID -> vm = weightViewModel
                    bloodPressureFragment_tabID -> vm = bloodPressureViewModel
                    diaryFragment_tabID -> vm = diaryViewModel
                }

                if (vm.getSizeFiltered() > 0) {
                    val alertDialogBuilder = AlertDialog.Builder(this)
                        .setCancelable(true)
                        .setNeutralButton(getString(string.cancel)) { dialog: DialogInterface, _: Int -> dialog.cancel() }
                        .setTitle(this.getString(string.warning))
                        .setMessage(this.getString(string.doYouReallyWantToContinue1) + " " + vm.getSizeFiltered() + " " + this.getString(string.doYouReallyWantToContinue2) + " " + vm.itemName + " " + this.getString(string.doYouReallyWantToContinue3))
                        .setPositiveButton(this.getString(string.yes)) { _, _ ->
                            Toast.makeText(this, vm.getSizeFiltered().toString() + " " + vm.itemName + " " + getString(string.recordsDeleted), Toast.LENGTH_SHORT).show()
                            vm.deleteAllFiltered()
                            // Todo need a different refresh, not working this way
                            //if (weightFragment != null) weightFragment?.notifyDataSetChanged()
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
                else Snackbar.make(findViewById(android.R.id.content), getString(string.noDataToDelete), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return true
            }
            R.id.action_restore -> {

                // Need to explicitly ask for permission once
                val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *PERMISSIONS)) {
                    ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                }

                // Pick backup folder
                var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
                chooseFile.type = "*/*"
                chooseFile = Intent.createChooser(chooseFile, this.getString(string.selectFile))
                startActivityForResult(chooseFile, rcRESTORE)
                return true
            }
            R.id.action_backup -> {

                // Need to explicitly ask for permission once
                val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *PERMISSIONS)) {
                    ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                }
                Snackbar.make(findViewById(android.R.id.content),this.getString(string.selectDirectory), Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                Toast.makeText(this, this.getString(string.selectDirectory), Toast.LENGTH_LONG).show()

                // Pick backup folder
                val i = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
                i.addCategory(Intent.CATEGORY_DEFAULT)
                startActivityForResult(Intent.createChooser(i, this.getString(string.selectDirectory)), rcBACKUP)
                return true
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //do here
            } else {
                Snackbar.make(findViewById(android.R.id.content), "E: MediLog  was not allowed to write to your file system.", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        if (data == null) return
        data ?: return

        // Backup section
        if (requestCode == rcBACKUP) {
            var backupFolderUri: Uri? = null

            // Check Uri is sound
            try {
                backupFolderUri = data.data
                Log.e("Debug", "Empty Uri $backupFolderUri")
            } catch (e: Exception) {
                Snackbar.make(findViewById(android.R.id.content),this.getString(string.eSelectDirectory) + " " + data, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                //Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
            if (backupFolderUri == null) {
                Snackbar.make(findViewById(android.R.id.content),this.getString(string.eSelectDirectory) + " " + data, Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                return
            }
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            val zipBackup = sharedPref.getBoolean(SettingsActivity.KEY_PREF_ZIPBACKUP, false)
            if (!zipBackup) {
                weightViewModel.exportCSV(backupFolderUri, weightViewModel.csvData(false))
                diaryViewModel.exportCSV(backupFolderUri, diaryViewModel.csvData(false))
                bloodPressureViewModel.exportCSV(backupFolderUri, bloodPressureViewModel.csvData(false))
            } else {
                createZIP(backupFolderUri)
            }
        }

        // Restore from backup
        if (requestCode == rcRESTORE) {
            var backupFileUri: Uri? = null
            try {
                backupFileUri = data.data
            } catch (e: Exception) {
                Snackbar.make(findViewById(android.R.id.content),this.getString(string.eSelectDirectory) + " " + data, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                //Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                //Log.e("Debug", "Invalid Uri: $e")
            }
            if (backupFileUri == null) {
                return
            }

            // Analyse file
            val cr = getContentResolver()
            val mimeType = cr.getType(backupFileUri)

            when (mimeType) {
                "application/zip" -> {
                    if (!openZIP(backupFileUri)) return
                }
                "application/csv",
                "text/csv",
                "text/comma-separated-values" -> { restoreData(backupFileUri)
                    return
                }
                else -> {
                    Snackbar.make(findViewById(android.R.id.content),"Don't know how to handle file type: " + mimeType, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                    //Toast.makeText(this, "Don't know how to handle file type: " + mimeType, Toast.LENGTH_LONG).show()
                    return
                }
            }

        }
    }

    private fun restoreData(backupFileUri: Uri) {
        val `in`: InputStream?
        try {
            `in` = contentResolver.openInputStream(backupFileUri)
            reader = BufferedReader(InputStreamReader(Objects.requireNonNull(`in`)))

        } catch (e: FileNotFoundException) {
            Snackbar.make(findViewById(android.R.id.content),this.getString(string.fileNotFound) + " " + backupFileUri, Snackbar.LENGTH_LONG).setAction("Action", null).show()
//            Toast.makeText(this, this.getString(string.fileNotFound) + " " + backupFileUri, Toast.LENGTH_LONG).show()
//                Log.e("Exception", "File not found: $e")
            return
        }

        // Valid header?
        try {
            val WEIGHT = 1
            val BLOODPRESSURE = 2
            val DIARY = 3
            val line = reader?.readLine()
            if (line == null) {
                Snackbar.make(findViewById(android.R.id.content),this.getString(string.eNoMediLogFile) + " " + line, Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                Toast.makeText(this, this.getString(string.eNoMediLogFile) + " " + line, Toast.LENGTH_SHORT).show()
                return
            }
//                val line2 = reader?.readLine()
            //               Log.d("--------------- Debug line", " -- $line")

            // Find out what we are importing
            var selector = -1
            if (line.indexOf(this.getString(string.weight)) > 0) {
                selector = WEIGHT
            }
            if (line.indexOf(this.getString(string.systolic)) > 0) {
                selector = BLOODPRESSURE
            }
            if (line.indexOf(this.getString(string.diary)) > 0) {
                selector = DIARY
            }

            var wantedHeader: String
            var vm: DataViewModel?
            when (selector) {
                WEIGHT -> {
                    vm = weightViewModel
                    wantedHeader = getString(string.date) + vm.separator + getString(string.weight)
                }
                BLOODPRESSURE -> {
                    vm = bloodPressureViewModel
                    wantedHeader = getString(string.date) + vm.separator + getString(string.systolic) + vm.separator + getString(string.diastolic) + vm.separator + getString(string.pulse)
                }
                DIARY -> {
                    vm = diaryViewModel
                    wantedHeader = getString(string.date) + vm.separator + getString(string.diary)
                }
                else -> {
                    Snackbar.make(findViewById(android.R.id.content),this.getString(string.eNoMediLogFile) + " " + line, 6000).setAction("Action", null).show()
                    return
                }
            }

            // Validate header line
            var headerLine = line.replace("\\s".toRegex(), "") // Remove potential blanks

            // Check if separator set in settings exists in headerLine
            if ( !vm.separator.isNullOrEmpty())
                if ( headerLine.indexOf(vm.separator!!) < 0) {
                    Snackbar.make(findViewById(android.R.id.content), this.getString(string.errorInCSVHeader) + " " + vm.separator + " " + this.getString(string.asSeparator) + ", " + this.getString(string.found) + " " + identifyCsvSeparator(headerLine), 6000).setAction("Action", null).show()
                    return
                }

            if ( headerLine.indexOf(wantedHeader) < 0) { // Wanted header not found = problem with the header
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.errorInCSVHeader) + ": '" + wantedHeader + "', " + this.getString(string.found) + ": '" + headerLine + "'", 6000).setAction("Action", null).show()
                return
            }

//            if (vm != null) {
                if (vm.getSizeUnfiltered() > 0) { // No need for warning if the db is empty
                    val alertDialogBuilder = AlertDialog.Builder(this)
                            .setTitle(this.getString(string.warning))
                            .setMessage(this.getString(string.doYouReallyWantToContinue1) + " " + vm.getSizeUnfiltered() + " " + this.getString(string.doYouReallyWantToContinue2) + " " + vm.itemName + " " + this.getString(string.doYouReallyWantToContinue3))
                            .setCancelable(true)
                            .setNeutralButton(getString(string.cancel)) { dialog: DialogInterface, _: Int -> dialog.cancel() }
                            .setPositiveButton(this.getString(string.yes)) { _, _ ->
                                vm.suspendedFileImport(reader, true)
                            }
                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                } else vm.suspendedFileImport(reader, true)
  //          }
        }
        catch (e: IOException) {
            Snackbar.make(findViewById(android.R.id.content),this.getString(string.eReadError), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            //Toast.makeText(this, this.getString(string.eReadError), Toast.LENGTH_SHORT).show()
//            Log.e("Exception", "File read failed: $e")
            return
        }
    }

    fun createZIP(uri: Uri): DocumentFile? {
        //######################### ZIP ##################################
        // https://github.com/srikanth-lingala/zip4j
        Log.e("Debug", "Folder: $uri")

        // Create empty Zip file
        val zipFile = "MediLog-Backup-" + SimpleDateFormat("yyyy-MM-dd").format(Date().time) + ".zip"
        var dFile: DocumentFile? = null
        try {
            val dFolder = DocumentFile.fromTreeUri(this, uri)
            //             Log.e("Debug", "isDirectory: " + dFolder.isDirectory());
            if (dFolder != null) dFile = dFolder.createFile("application/zip", zipFile)
            if (dFile == null) {
                Toast.makeText(this, this.getString(string.eCreateFile) + ": " + dFile, Toast.LENGTH_LONG).show()
                Log.e("Debug", "ieFile: $dFile")
                return null
            }
        } catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eCreateFile) + ": " + dFile + " " + e.toString(), Toast.LENGTH_LONG).show()
            Log.e("Exception", "Can't create file: $e")
            return null
        }

//        dFile = DocumentFile.fromTreeUri(this, uri);

        // Add files to ZIP file
        val dest: OutputStream?
        val out: ZipOutputStream?
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val zipPassword: String? = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD, "MediLog")
        try {
            dest = contentResolver.openOutputStream(dFile.uri)
            Log.e("Debug", "FileOutputStream: $dest")
            out = if (zipPassword!!.isEmpty()) {
                ZipOutputStream(BufferedOutputStream(dest))
            } else {
                ZipOutputStream(BufferedOutputStream(dest), zipPassword.toCharArray())
            }
            Log.e("Debug", "ZipOutStream: $out")
            Log.v("Compress", "Adding: ")
        } catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return null
        }
        val zipParameters = ZipParameters()
        if (zipPassword.length > 0) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }
        val fileNames = arrayOfNulls<String>(3)
        fileNames[0] = weightViewModel.fileName
        fileNames[1] = bloodPressureViewModel.fileName
        fileNames[2] = diaryViewModel.fileName
        val zipData = arrayOfNulls<String>(3)
        zipData[0] = weightViewModel.csvData(false)
        zipData[1] = bloodPressureViewModel.csvData(false)
        zipData[2] = diaryViewModel.csvData(false)
        for (i in 0..2) {
            try {
                zipParameters.fileNameInZip = fileNames[i]
                out.putNextEntry(zipParameters)
                out.write(zipData[i]!!.toByteArray())
                out.closeEntry()
            } catch (e: Exception) {
                Toast.makeText(this, this.getString(string.eCreateFile) + " " + fileNames[i], Toast.LENGTH_LONG).show()
                e.printStackTrace()
                return null
            }
        }
        try {
            out.close()
        } catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eCreateFile) + " 2 " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return null
        }
        if (zipPassword.length == 0) {
            Toast.makeText(this, this.getString(string.fileCreated) + " " + dFile.name, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, this.getString(string.protectedZipFileCreated) + " " + dFile.name, Toast.LENGTH_LONG).show()
        }

        // Done with Backup
        return dFile
    }

    fun openZIP(zipUri: Uri): Boolean {
        Toast.makeText(this, "Restoring from ZIP files is not yet supported", Toast.LENGTH_LONG).show()
/*
        val zipFile = ZipFile(File(zipUri.getPath()))
        if (zipFile.isEncrypted()) {
            Toast.makeText(this, "Restoring from encrypted ZIP files not yet supported", Toast.LENGTH_LONG).show()
            }
        else {
            val fileHeaders = zipFile.getFileHeaders()
            if (fileHeaders.size == 0) Toast.makeText(this, "ZIP file seems to be empty?", Toast.LENGTH_LONG).show()

            fileHeaders.stream().forEach { fileHeader ->
                val inputStream: InputStream = zipFile.getInputStream(fileHeader)
                Toast.makeText(this, "Found: " + fileHeader.fileName, Toast.LENGTH_LONG).show()
             }
        }
//        Extracting All files in a password protected zip
//        new ZipFile("filename.zip", "password".toCharArray()).extractAll("/destination_directory");*/
        return false
    }

    private fun identifyCsvSeparator(header: String): String {
        val acceptableSeparators = this.getString(string.acceptableCsvSeparators)

        for (i in 0..acceptableSeparators.length-1)
            if (header.indexOf(acceptableSeparators[i]) > 0) return acceptableSeparators[i].toString()
        return " "
    }


    // #####################################################
    // Application independent code
    // #####################################################
    public override fun onPause() {
        super.onPause()
        // Save active tab
//        Log.d("OnPause", "activeTab " + tabLayout!!.selectedTabPosition)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPref.edit()
        editor.putInt("activeTab", tabLayout.selectedTabPosition)
        editor.apply()
    }

    public override fun onStop() {
        super.onStop()
        // Save active tab
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPref.edit()
        editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
        editor.apply()
    }

    public override fun onDestroy() {
        super.onDestroy()
        // Save active tab
    }

    private fun setTheme() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        when (sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(string.blue))) {
            this.getString(string.green) -> theme.applyStyle(style.AppThemeGreenNoBar, true)
            this.getString(string.blue)  -> theme.applyStyle(style.AppThemeBlueNoBar, true)
            this.getString(string.red)   -> theme.applyStyle(style.AppThemeRedNoBar, true)
        }
    }

    companion object {
        private const val REQUEST = 112
        private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false
                    }
                }
            }
            return true
        }

        fun resetReAuthenticationTimer(context: Context?) {
            // Reset Stopwatch, after all we are still in MediLog
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = sharedPref.edit()
            editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
            editor.apply()
        }

    }
}