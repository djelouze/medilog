package com.zell_mbc.medilog.base

import com.zell_mbc.medilog.DataItem

abstract class DataRepository() {

    abstract suspend fun delete(id: Int)
    abstract suspend fun deleteAll()
    abstract suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long)

    abstract fun getSizeUnfiltered(): Int
    abstract fun getSizeFiltered(filterStart: Long, filterEnd: Long): Int
    abstract fun getItem(index: Int): DataItem
    abstract fun getItems(order: String): List<DataItem>
    abstract fun getItemsFiltered(order: String, filterStart: Long, filterEnd: Long): List<DataItem>
}