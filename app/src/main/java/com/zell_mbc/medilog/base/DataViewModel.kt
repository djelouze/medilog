package com.zell_mbc.medilog.base

import android.app.Application
import android.content.SharedPreferences
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Environment
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.DataItem
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.SettingsActivity.Companion.KEY_PREF_DELIMITER
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

abstract class DataViewModel(application: Application): AndroidViewModel(application) {
    val app = application

    abstract val fileName: String
    abstract var itemName: String

    abstract val filterStartPref: String
    abstract val filterEndPref: String
    var filterStart = 0L
    var filterEnd = 0L
    var filterActive = false

//    lateinit var itemsX: LiveData<List<T>>

    abstract val repository: DataRepository

    @JvmField
    val sharedPref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    @JvmField
    val separator = sharedPref.getString(KEY_PREF_DELIMITER, ",")
    var csvPattern = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    abstract fun createPDF(): PdfDocument?
    abstract fun csvData(filtered: Boolean): String
    abstract suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int

    fun toStringDate(l: Long): String {
        return DateFormat.getDateInstance().format(l)
    }

    fun toStringTime(l: Long): String {
        return SimpleDateFormat("HH:mm").format(l)
    }

    // PDF margin defaults
    val pdfHeaderTop = 30
    val pdfHeaderBottom = 50
    val pdfDataTop = 70
    val pdfLineSpacing = 15
    val pdfLeftBorder = 10
    val pdfTimeTab = 80 // Left offset for time
    val pdfDataTab = 150

    var pdfRightBorder = 0 // Set in fragment based on canvas size - pdfLeftBorder
    var pdfDataBottom = 0 // Set in fragment based on canvas size

    fun delete(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(id)
    }


    // Resets the filter an deletes all rows
    fun deleteAllUnfiltered() = viewModelScope.launch(Dispatchers.IO) {
        resetFilter()
        repository.deleteAll()
        // Todo: Figure out how to refresh fragment after filter has been reset
    }

    // Resets the filter an deletes all rows
    fun deleteAllFiltered() = viewModelScope.launch(Dispatchers.IO) {
        if (filterActive) repository.deleteAllFiltered(filterStart, filterEnd)
    }

    abstract fun getItem(id: Int): DataItem?
    abstract fun getItems(order: String): List<DataItem>

    fun getSizeFiltered(): Int {
        getFilter()
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) { i = repository.getSizeFiltered(filterStart, filterEnd) }
            j.join()
        }
        return i
    }

    fun getSizeUnfiltered(): Int {
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) { i = repository.getSizeUnfiltered() }
            j.join()
        }
        return i
    }


    // Helper functions
    open fun init() {
        getFilter()
    }

    fun getFilter() {
        filterStart = sharedPref.getLong(filterStartPref, 0L)
        filterEnd = sharedPref.getLong(filterEndPref, 0L)
        filterActive = filterStart + filterEnd != 0L
        if (filterEnd == 0L) filterEnd = Calendar.getInstance().timeInMillis + (86400000 * 100) // Add a 100 days worth of milliseconds to make sure we capture all of todays items
    }

    fun setFilter(start: Long, end: Long) {
        val editor = sharedPref.edit()
        editor.putLong(filterStartPref, start)
        editor.putLong(filterEndPref, end)
        editor.apply()

        filterActive = start + end != 0L
    }

    fun resetFilter() {
        val editor = sharedPref.edit()
        editor.putLong(filterStartPref, 0L)
        editor.putLong(filterEndPref, 0L)
        editor.apply()
        filterActive = false
    }


    fun suspendedFileImport(reader: BufferedReader?, replaceData: Boolean) {
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = importFile(reader, replaceData)
            }
            j.join()
        }

        when (i) {
            -1 -> {
                Toast.makeText(app, app.getString(R.string.eReadError), Toast.LENGTH_SHORT).show()
            } // IO Exception
            -2 -> {
                Toast.makeText(app, app.getString(R.string.eValueFormat) + ", no Data imported", Toast.LENGTH_SHORT).show()
            } // No lines to import
            else -> {
                Toast.makeText(app, i.toString() + " " + itemName + " " + app.getString(R.string.recordsLoaded), Toast.LENGTH_SHORT).show()
            }
        }
    }


    fun getPDF(document: PdfDocument?): Uri? {
        if (document == null) {
            return null
        }
//        val fn = fileName!!.replace("csv", "pdf")
        val filePath = File(app.cacheDir, "")
        val newFile = File(filePath, fileName.replace("csv", "pdf"))
        val uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.writeTo(out)
                out.flush()
                out.close()
            }
        } catch (e: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }
        return uri
    }

    fun exportCSV(backupFolderUri: Uri?, csv: String) {
        var dFile: DocumentFile? = null
        try {
            val dFolder = DocumentFile.fromTreeUri(app, backupFolderUri!!)
            //             Log.e("Debug", "isDirectory: " + dFolder.isDirectory());
            val fn = fileName.replace(".", "-" + SimpleDateFormat("yyyy-MM-dd").format(Date().time) + ".")
            if (dFolder != null) dFile = dFolder.createFile("application/csv", fn)
            if (dFile == null) {
                Toast.makeText(app, app.getString(R.string.eCreateFile) + ": " + dFile, Toast.LENGTH_LONG).show()
                return
            }
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString(), Toast.LENGTH_LONG).show()
            return
        }
        val o: OutputStream?
        o = try {
            app.contentResolver.openOutputStream(dFile.uri)
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return
        }
        if (o == null) return
        try {
            o.write(csv.toByteArray())
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eWriteError) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return
        }
        try {
            o.close()
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eCloseFile) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return
        }

        Toast.makeText(app, app.getString(R.string.fileCreated) + " " + dFile.name + "\n" + csv.lines().count() + " " + app.getString(R.string.itemsExported), Toast.LENGTH_LONG).show()
    }

    fun getZIP(document: PdfDocument?): Uri? {
        if (document == null) {
            Toast.makeText(app, fileName.replace(".csv","") + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        // https://github.com/srikanth-lingala/zip4j

        // Create temporary file
        var fn = fileName.replace("csv", "pdf")
        var filePath = File(app.externalCacheDir, "")
        val newFile = File(filePath, fn)
        var uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.writeTo(out)
                out.flush()
                out.close()
            }
        } catch (e: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }

        // Create ZIP File
//        val sharedPref = PreferenceManager.getDefaultSharedPreferences(app)
        val zipPassword = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD, "MediLog")
        val zipParameters = ZipParameters()
        if (zipPassword!!.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }
        filePath = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "")
//        filePath = app.getFilesDir()
        fn = filePath.toString() + "/MediLog-" + fileName.replace(".csv", "") + "-" + SimpleDateFormat("yyyy-MM-dd").format(Date().time) + ".zip"
        val zf: ZipFile
        zf = if (zipPassword.isEmpty()) {
            ZipFile(fn)
        } else {
            ZipFile(fn, zipPassword.toCharArray())
        }
        try {
            zf.addFile(newFile.toString(), zipParameters)
        } catch (e: IOException) {
            Toast.makeText(app, app.getString(R.string.eShareError) + " " + zf, Toast.LENGTH_LONG).show()
            return null
        }

        // Delete tmpFile
        newFile.delete()
        val fileWithinMyDir = File(fn)
        if (fileWithinMyDir.exists()) {
            uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", fileWithinMyDir)
            if (zipPassword.isEmpty()) { Toast.makeText(app, app.getString(R.string.fileCreated) + " " + zf, Toast.LENGTH_LONG).show() }
            return uri
        }
        return null
    }
}