package com.zell_mbc.medilog

import android.R.attr.path
import android.app.AlertDialog
import android.app.Application
import android.content.res.Configuration
import android.util.Log
import androidx.preference.PreferenceManager


class MediLog : Application() {
    internal var Authenticated: Boolean = false
    internal var hasBiometric = true

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    override fun onCreate() {
        super.onCreate()
//        Log.d("MediLog Application", "onCreate")

      val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        // If Authenticated is false lets check if we want to and if this device can authenticate
      if (!Authenticated) {
          if (!sharedPref.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
              Authenticated = true         // If user set biometric login off, we are always authenticated
          } else { // Biometric setting is on -> Let's check if authentication is possible
              val biometricHelper = BiometricHelper(this)
              // Check if biometric device  exists, if not remove biometric setting in settings activity and set authenticated to always true
              if (biometricHelper.hasHardware(false)) {
                  Authenticated = false    // Make sure we launch the Biometric logon process in onStart
              } else {
                  Authenticated = true
                  hasBiometric = false
              }
          }
      }

      val Theme = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
      // Set to default on first launch
      if (Theme == "") {
          val editor = sharedPref.edit()
          editor.putString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
          editor.apply()
      }
  }

  // Called by the system when the device configuration changes while your component is running.
  // Overriding this method is totally optional!
  override fun onConfigurationChanged ( newConfig : Configuration) {
      super.onConfigurationChanged(newConfig)
//      Log.d("MediLog Application", "onConfigurationChange")
  }

    fun setAuthenticated (b: Boolean) { Authenticated = b }
    fun isAuthenticated () : Boolean { return Authenticated }
}

